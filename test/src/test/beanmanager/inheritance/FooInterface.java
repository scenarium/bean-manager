package test.beanmanager.inheritance;

public interface FooInterface {
	String toString();

	int getVal();
}

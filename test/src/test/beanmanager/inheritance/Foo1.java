package test.beanmanager.inheritance;

public class Foo1 implements FooInterface {
	private int val;

	public Foo1(int val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return Foo1.class.getSimpleName() + " " + val;
	}

	public int getVal() {
		return val;
	}
}

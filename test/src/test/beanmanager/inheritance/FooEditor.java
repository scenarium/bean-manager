package test.beanmanager.inheritance;

import io.beanmanager.editors.PropertyEditor;

public class FooEditor extends PropertyEditor<FooInterface> {

	@Override
	public String getAsText() {
		return getValue().toString();
	}

	@Override
	public void setAsText(String text) {
		String[] subSeq = text.split(" ");
		int val = Integer.parseInt(subSeq[1]);
		setValue(subSeq[0].equals(Foo1.class.getSimpleName()) ? new Foo1(val) : new Foo2(val));
	}
}

package test.beanmanager.inheritance;

public class Foo2 implements FooInterface {
	private int val;

	public Foo2(int val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return Foo2.class.getSimpleName() + " " + val;
	}

	public int getVal() {
		return val;
	}
}

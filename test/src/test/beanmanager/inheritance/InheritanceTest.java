package test.beanmanager.inheritance;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.ihmtest.FxTest;
import org.junit.jupiter.api.Test;

public class InheritanceTest {
	private Foo1 fooInterface;

	@Test
	public void recordWithoutWithEditorInInterface() throws IOException {
		PropertyEditorManager.registerEditor(FooInterface.class, FooEditor.class);
		InheritanceTest testedObject = new InheritanceTest();
		testedObject.setFooInterface(new Foo1(7));
		BeanManager bm = new BeanManager(testedObject, "");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.save(baos, "");
		InheritanceTest testedObject2 = new InheritanceTest();
		BeanManager bml = new BeanManager(testedObject2, "");
		bml.load(new ByteArrayInputStream(baos.toByteArray()), "");
		System.out.println(testedObject2.fooInterface);
		assertTrue(testedObject.fooInterface.getVal() == testedObject2.fooInterface.getVal());
		new FxTest().launchIHM(new String[0], s -> bm.getEditor());
	}

	public Foo1 getFooInterface() {
		return fooInterface;
	}

	public void setFooInterface(Foo1 fooInterface) {
		this.fooInterface = fooInterface;
	}
}

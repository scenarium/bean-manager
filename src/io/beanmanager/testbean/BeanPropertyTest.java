package io.beanmanager.testbean;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.BeanInfo;
import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class BeanPropertyTest {
	@PropertyInfo(nullable = true)
	@BeanInfo(inline = true)
	private SubCarotte inlineCarotte;
	@PropertyInfo(nullable = false)
	@BeanInfo(inline = true)
	private SubCarotte inlineNonNullableCarotte;

	@PropertyInfo(nullable = true)
	@BeanInfo(inline = true, possibleSubclasses = { MoutonBlanc.class, MoutonNoir.class })
	private AbstractMouton inlineMouton;
	@PropertyInfo(nullable = false)
	@BeanInfo(inline = true, possibleSubclasses = { MoutonBlanc.class, MoutonNoir.class })
	private AbstractMouton inlineNonNullableMouton;

	private SubCarotte sharedCarotte;

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new BeanManager(new BeanPropertyTest(), "").getEditor()), true);
	}

	public SubCarotte getInlineCarotte() {
		return this.inlineCarotte;
	}

	public void setInlineCarotte(SubCarotte inlineCarotte) {
		this.inlineCarotte = inlineCarotte;
	}

	public SubCarotte getInlineNonNullableCarotte() {
		return this.inlineNonNullableCarotte;
	}

	public void setInlineNonNullableCarotte(SubCarotte inlineNonNullableCarotte) {
		this.inlineNonNullableCarotte = inlineNonNullableCarotte;
	}

	public SubCarotte getSharedCarotte() {
		return this.sharedCarotte;
	}

	public void setSharedCarotte(SubCarotte sharedCarotte) {
		this.sharedCarotte = sharedCarotte;
	}

	public AbstractMouton getInlineMouton() {
		return this.inlineMouton;
	}

	public void setInlineMouton(AbstractMouton inlineMouton) {
		this.inlineMouton = inlineMouton;
	}

	public AbstractMouton getInlineNonNullableMouton() {
		return this.inlineNonNullableMouton;
	}

	public void setInlineNonNullableMouton(AbstractMouton inlineNonNullableMouton) {
		this.inlineNonNullableMouton = inlineNonNullableMouton;
	}
}

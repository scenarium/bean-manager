/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.testbean.inheritance;

import io.beanmanager.BeanManager;
import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class Test {
	public static void main(String[] args) {
		BeanManager bm = new BeanManager(new Test(), "");
		FxTest.launchIHM(args, s -> new VBox(bm.getEditor()));
	}

	private Child child;

	public Child getChild() {
		return this.child;
	}

	public void setChild(Child child) {
		this.child = child;
	}
}

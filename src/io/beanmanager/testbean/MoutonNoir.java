/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.testbean;

import javax.vecmath.Color4f;

public class MoutonNoir extends MoutonBlanc {
	private static final long serialVersionUID = 1L;
	private Color4f color = new Color4f(0, 0, 0, 1);
	private boolean isRapide;

	@Override
	public Color4f getColor() {
		return this.color;
	}

	public boolean isRapide() {
		return this.isRapide;
	}

	@Override
	public void setColor(Color4f color) {
		this.color = color;
	}

	public void setRapide(boolean isRapide) {
		this.isRapide = isRapide;
	}
}

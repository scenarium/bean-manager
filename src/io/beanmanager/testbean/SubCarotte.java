/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.testbean;

public class SubCarotte extends Carotte {
	private static final long serialVersionUID = 1L;
	private double ours;

	public double getOurs() {
		return this.ours;
	}

	public void setOurs(double ours) {
		this.ours = ours;
	}
}

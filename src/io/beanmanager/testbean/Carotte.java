/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.testbean;

import java.io.Serializable;

import io.beanmanager.editors.container.BeanInfo;

public class Carotte implements Serializable {
	private static final long serialVersionUID = 1L;
	private float couleur;
	private int saveur;
	private AbstractMouton mouton = new MoutonBlanc();
	@BeanInfo(possibleSubclasses = SubCarotte.class)
	private Carotte car2;
	private Carotte car;
	private C1 c1 = new C1();

	public C1 getC1() {
		return this.c1;
	}

	public Carotte getCar() {
		return this.car;
	}

	public Carotte getCar2() {
		return this.car2;
	}

	public float getCouleur() {
		return this.couleur;
	}

	public AbstractMouton getMouton() {
		return this.mouton;
	}

	public int getSaveur() {
		return this.saveur;
	}

	public void setC1(C1 c1) {
		this.c1 = c1;
	}

	public void setCar(Carotte car) {
		this.car = car;
	}

	public void setCar2(Carotte car2) {
		this.car2 = car2;
	}

	public void setCouleur(float couleur) {
		this.couleur = couleur;
	}

	public void setMouton(AbstractMouton mouton) {
		this.mouton = mouton;
	}

	public void setSaveur(int saveur) {
		this.saveur = saveur;
	}
}

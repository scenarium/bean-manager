/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.testbean;

import javax.vecmath.Color4f;

public class MoutonBlanc extends AbstractMouton {
	private static final long serialVersionUID = 1L;
	private Color4f color = new Color4f(1, 1, 1, 1);
	private boolean isRaciste;

	public Color4f getColor() {
		return this.color;
	}

	public boolean isRaciste() {
		return this.isRaciste;
	}

	public void setColor(Color4f color) {
		this.color = color;
	}

	public void setRaciste(boolean isRaciste) {
		this.isRaciste = isRaciste;
	}
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface PropertyInfo {
	/** Defines whether this is an expert property
	 * @return true if this is an expert property */
	boolean expert() default false;

	/** Defines whether this is a hidden property
	 * @return true of this is a hidden property */
	boolean hidden() default false;

	/** Index used to sort properties in the GUI
	 * @return index of the property */
	int index() default -1;

	/** Literal description
	 * @return literal description */
	String info() default "";

	/** Defines whether this property can be set to null
	 * @return true if this property can be set to null */
	boolean nullable() default true;

	/** Defines whether this property is read-only and therefore cannot be set
	 * @return true if this property can be set to null */
	boolean readOnly() default false;

	/** Defines whether this property is savable and therefore cannot be saved in a property file
	 * @return true if this property is savable */
	boolean savable() default true;

	/** Literal description of the unit of this property
	 * @return literal description of the unit */
	String unit() default "";

	/** Defines whether this property is viewable and therefore can be shown in a GUI
	 * @return true if this property is viewable */
	boolean viewable() default true;

	/** Defines whether this property is editable and therefore can be edited with the GUI
	 * @return true if this property is editable */
	boolean editable() default true;
}

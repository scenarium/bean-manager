/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.time;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.time.LocalDateTime;

import io.beanmanager.editors.PropertyChangeListener;
import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.ihmtest.FxTest;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class LocalDateTimeEditor extends PropertyEditor<LocalDateTime> {
	private final LocalDateEditor dateEditor = new LocalDateEditor();
	private final LocalTimeEditor timeEditor = new LocalTimeEditor();

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> {
			VBox vbox = new VBox(new LocalDateTimeEditor().getCustomEditor(), new LocalDateTimeEditor().getCustomEditor());
			vbox.setAlignment(Pos.CENTER);
			return vbox;
		});
	}

	public LocalDateTimeEditor() {
		PropertyChangeListener pcl = () -> super.setValue(
				this.dateEditor.getValue() != null && this.timeEditor.getValue() != null ? LocalDateTime.of(this.dateEditor.getValue(), this.timeEditor.getValue()) : null);
		this.dateEditor.addPropertyChangeListener(pcl);
		this.timeEditor.addPropertyChangeListener(pcl);
	}

	public LocalDateTimeEditor(boolean hoor, boolean minute, boolean second, boolean nanosecond) {
		this();
		setFilter(hoor, minute, second, nanosecond);
	}

	@Override
	public String getAsText() {
		return getValue() == null ? "" : getValue().toString();
	}

	@Override
	protected Region getCustomEditor() {
		Region dateRegion = this.dateEditor.getCustomEditor();
		HBox.setHgrow(dateRegion, Priority.ALWAYS);
		Region timeRegion = this.timeEditor.getCustomEditor();
		HBox.setHgrow(timeRegion, Priority.ALWAYS);
		HBox box = new HBox(dateRegion, timeRegion);
		box.setMaxWidth(Double.MAX_VALUE);
		return box;
	}

	@Override
	public int getPitch() {
		return this.dateEditor.getPitch() + this.timeEditor.getPitch();
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return this.dateEditor.isFixedControlSized() && this.timeEditor.isFixedControlSized();
	}

	@Override
	public LocalDateTime readValue(DataInput raf) throws IOException {
		return LocalDateTime.of(this.dateEditor.readValue(raf), this.timeEditor.readValue(raf));
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() ? null : LocalDateTime.parse(text));
	}

	public void setFilter(boolean hour, boolean minute, boolean second, boolean nanosecond) {
		this.timeEditor.setFilter(hour, minute, second, nanosecond);
	}

	@Override
	public void updateCustomEditor() {
		if (this.dateEditor == null)
			return;
		LocalDateTime time = getValue();
		if (time == null) {
			if (this.dateEditor.getValue() != null)
				this.dateEditor.setValue(null);
			if (this.timeEditor.getValue() != null)
				this.timeEditor.setValue(null);
		} else {
			if (!time.toLocalDate().equals(this.dateEditor.getValue()))
				this.dateEditor.setValue(time.toLocalDate());
			if (!time.toLocalTime().equals(this.timeEditor.getValue()))
				this.timeEditor.setValue(time.toLocalTime());
		}
	}

	@Override
	public void writeValue(DataOutput raf, LocalDateTime value) throws IOException {
		this.dateEditor.writeValue(raf, value.toLocalDate());
		this.timeEditor.writeValue(raf, value.toLocalTime());
	}
}

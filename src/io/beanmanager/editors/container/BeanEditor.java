/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.container;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

import io.beanmanager.BeanCreatedListener;
import io.beanmanager.BeanDesc;
import io.beanmanager.BeanListChangeListener;
import io.beanmanager.BeanManager;
import io.beanmanager.BeanRegisterListener;
import io.beanmanager.BeanRenameListener;
import io.beanmanager.BeanUnregisterListener;
import io.beanmanager.editors.DynamicSizeEditor;
import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.ihmtest.FxTest;
import io.beanmanager.internal.LoadPackageStream;
import io.beanmanager.internal.Log;
import io.beanmanager.testbean.TypeTest;
import io.beanmanager.tools.EventListenerList;
import io.beanmanager.tools.FxUtils;
import io.scenarium.pluginManager.LoadModuleListener;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class BeanEditor<T> extends GenericPropertyContainerEditor<T, T> implements BeanRegisterListener, BeanUnregisterListener, DynamicSizeEditor, BeanRenameListener, LoadModuleListener {
	public static boolean needToLoadBrotherBean = true;
	private static BeanInstancesMap beanInstances = new BeanInstancesMap();
	private static final EventListenerList<BeanCreatedListener> BEANS_CREATED_LISTENERS = new EventListenerList<>();
	private static final EventListenerList<BeanRegisterListener> BEANS_REGISTERED_LISTENERS = new EventListenerList<>();
	private static final EventListenerList<BeanRenameListener> BEANS_RENAMED_LISTENERS = new EventListenerList<>();
	private static final EventListenerList<BeanUnregisterListener> BEANS_UNREGISTERED_LISTENERS = new EventListenerList<>();

	private static final String NEWINSTANCE = "New instance...";
	private static final String NULL = String.valueOf((Object) null);
	static Function<Object, Image> iconProvider;
	private final boolean inline;
	private final EventListenerList<BeanListChangeListener> beansListlisteners = new EventListenerList<>();
	private Class<?>[] subClasses;
	private ComboBox<Object> comboBox;
	private Label objectLabel;

	public BeanEditor(Class<T> type, String local) {
		this(type, local, false, null);
	}

	public BeanEditor(Class<T> type, String local, boolean inline, Class<?>[] subClasses) {
		super(type, local);
		this.inline = inline;
		setSubClasses(subClasses);
	}

	@Override
	public void beanRegister(BeanDesc<?> beanDesc) {
		if (this.subClasses != null) {
			Class<? extends Object> beanClass = beanDesc.bean.getClass();
			for (int i = 0; i < this.subClasses.length; i++)
				if (this.subClasses[i] == beanClass) {
					FxUtils.runLaterIfNeeded(() -> updateModel((HBox) customEditor()));

					// Log.info("changement");
					return;
				}
		}
	}

	@Override
	public void beanUnregister(BeanDesc<?> beanDesc, boolean delete) {
		if (beanDesc.bean == getValue())
			setValue(null);
		if (this.subClasses != null) {
			Class<? extends Object> beanClass = beanDesc.bean.getClass();
			for (int i = 0; i < this.subClasses.length; i++)
				if (this.subClasses[i] == beanClass) {
					updateModel((HBox) customEditor());
					// Log.info("changement");
					return;
				}
		}
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		if (this.comboBox != null) {
			ObservableList<Object> items = this.comboBox.getItems();
			Object bean = beanDesc.bean;
			for (Object item : items)
				if (item instanceof BeanDesc<?> && ((BeanDesc<?>) item).bean == bean) {
					updateModel((HBox) customEditor());
					return;
				}
		}
	}

	public PropertyEditor<T> copyEditor() {
		BeanEditor<T> oe = new BeanEditor<>(getGenericComponentType(), this.local);
		oe.setSubClasses(this.subClasses);
		return oe;
	}

	public Object createDefaultObject() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		return this.subClasses == null || this.subClasses.length == 0 ? null : this.subClasses[0].getConstructor().newInstance();
	}

	@Override
	public String getAsText() {
		Object value = getValue();
		if (value == null)
			return null;
		Class<?> type = value.getClass();
		BeanList<?> beans = beanInstances.get(type);
		if (beans != null) {
			for (BeanDesc<?> beanDesc : beans)
				if (beanDesc.bean == value)
					return beanDesc.getSimpleDescriptor();
		} else
			Log.error("pas la");
		return null;
	}

	@Override
	protected Region getCustomEditor() {
		BeanManager.addWeakRefModuleLoadedListener(this);
		HBox hBox = new HBox();
		hBox.setSpacing(3);
		hBox.setAlignment(Pos.CENTER_LEFT);
		populateEditor(hBox);
		return hBox;
	}

	@Override
	public LinkedList<Object> getSubBeans() {
		Object subBean = getValue();
		if (subBean != null) {
			LinkedList<Object> subBeanList = new LinkedList<>();
			subBeanList.add(subBean);
			return subBeanList;
		}
		return null;
	}

	private Class<?> getType(String beanTypeName) {
		int moduleSeparator = beanTypeName.indexOf(BeanManager.MODULE_SEPARATOR);
		String moduleName;
		if (moduleSeparator != -1) {
			moduleName = beanTypeName.substring(0, moduleSeparator);
			beanTypeName = beanTypeName.substring(moduleSeparator + 1);
		} else
			moduleName = null;
		if (this.subClasses != null && !beanTypeName.equals(getGenericComponentType().getSimpleName())) {
			for (Class<?> c : this.subClasses)
				if (c.getSimpleName().equals(beanTypeName) && (moduleName == null || moduleName.equals(c.getModule().getName())))
					return c;
			return null;
		}
		return getGenericComponentType();
	}

	public boolean hasTypeChoice() {
		return this.subClasses != null;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public void initPatternEditorFactory(Function<Class<?>, PropertyEditor<?>> patternEditorFactory) {}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public boolean isInline() {
		return this.inline;
	}

	private void populateEditor(HBox hBox) {
		ObservableList<Node> children = hBox.getChildren();
		children.clear();
		if (this.inline || getValue() != null && getBeanDesc(getValue()) == null) {
			this.objectLabel = new Label(getValue() == null ? String.valueOf((Object) null) : getValue().toString());
			this.objectLabel.setAlignment(Pos.CENTER_LEFT);
			HBox.setHgrow(this.objectLabel, Priority.ALWAYS);
			this.objectLabel.setMaxWidth(Double.MAX_VALUE);
			children.add(this.objectLabel);
			if (this.inline) {
				ArrayList<Button> buttonList = new ArrayList<>();
				if (nullable() || !nullable() && getValue() == null) {
					Button existenceButton = new Button(getValue() == null ? "New" : "Drop");
					existenceButton.setOnAction(e -> {
						if (existenceButton.getText().equals("New")) {
							if (this.subClasses.length > 1)
								setNewValueFromSubClassesPossibilities(hBox.getScene().getWindow());
							else
								setNewValueFromDefaultObject();
						} else
							setValue(null);
						updateCustomEditor(hBox);
						fireSizeChanged();
					});
					buttonList.add(existenceButton);
				}
				if (getValue() != null && this.subClasses.length > 1) {
					Button replaceButton = new Button("Replace");
					replaceButton.setOnAction(e -> setNewValueFromSubClassesPossibilities(hBox.getScene().getWindow()));
					buttonList.add(replaceButton);
				}
				children.addAll(buttonList);
			} else {
				Button registerButton = new Button("Register");
				registerButton.setOnAction(e -> {
					if (BeanEditor.getBeanDesc(getValue()) == null) {
						fireBeanCreatedListener(BeanEditor.registerBean(getValue(), this.local));
						fireBeanListChange();
					}
					updateCustomEditor(hBox);
				});
				children.add(registerButton);
				if (nullable()) {
					Button dropButton = new Button("Drop");
					dropButton.setOnAction(e -> {
						setValueFromObj(nullable() || this.subClasses == null ? null : this.subClasses[0]);
						updateCustomEditor(hBox);
					});
					children.add(dropButton);
				}
			}
			if (this.comboBox != null) {
				removeWeakRefBeanRegisterListener(this);
				removeWeakRefBeanRenameListener(this);
				removeWeakRefBeanUnregisterListener(this);
				this.comboBox = null;
			}
		} else {
			this.comboBox = new ComboBox<>();
			this.comboBox.setCellFactory(lv -> new ObjectListcell());
			this.comboBox.setButtonCell(new ObjectListcell());
			updateModel(hBox);
			addWeakRefBeanRegisterListenerIfAbsent(this);
			addWeakRefBeanRenameListener(this);
			addWeakRefBeanUnregisterListenerIfAbsent(this);
			this.comboBox.setOnKeyPressed(e -> {
				if (e.getCode() == KeyCode.ENTER) {
					FxUtils.traverse();
					e.consume();
				}
			});
			this.comboBox.setOnScroll(e -> {
				if (e.getDeltaY() != 0) {
					ObservableList<Object> items = this.comboBox.getItems();
					items.indexOf(this.comboBox.getValue());
					int index = items.indexOf(this.comboBox.getValue()) + (e.getDeltaY() > 0 ? -1 : 1);
					if (index < 0)
						index = items.size() - 2;
					else if (index >= items.size() - 1)
						index = 0;
					this.comboBox.getSelectionModel().select(index);
				}
				e.consume();
			});
			this.comboBox.valueProperty().addListener(e -> {
				if (!this.comboBox.getItems().isEmpty())
					updateComboboxValue();
			});
			this.comboBox.setMinWidth(100);
			this.comboBox.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(this.comboBox, Priority.ALWAYS);
			hBox.getChildren().add(this.comboBox);
			this.objectLabel = null;
		}
	}

	@Override
	public T readValue(DataInput raf) throws IOException {
		try {
			String cn = raf.readUTF();
			if (!BeanManager.getClassFromDescriptor(cn).equals(getGenericComponentType())) {
				Log.error("Cannot read the class: " + cn + " from an ObjectEditor of type: " + getGenericComponentType());
				return null;
			}
			T val = getGenericComponentType().getDeclaredConstructor().newInstance();
			byte[] byteArray = new byte[raf.readInt()];
			raf.readFully(byteArray);
			new BeanManager(val, "").load(new ByteArrayInputStream(byteArray), "");
			return val;
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void removeSubBeans(Object beanToRemove) {
		if (getValue() == beanToRemove)
			setValue(null);
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			Object value = null;
			String beanFileName = null;
			String beanName = null;
			if (text != null && !text.equals(String.valueOf((Object) null)) && !text.isEmpty()) {
				beanFileName = text + BeanManager.SERIALIZE_EXT;
				int indexofti = text.indexOf(BeanDesc.SEPARATOR);
				if (indexofti == -1)
					return;
				beanName = text.substring(indexofti + 1);
				Class<?> type = getType(text.substring(0, indexofti));
				if (type == null)
					Log.error("Impossible to assign: " + text.substring(0, indexofti) + " to: " + getGenericComponentType());
				else {
					BeanDesc<?> beanDesc = BeanEditor.getRegisterBean(type, beanName);
					if (beanDesc == null) {
						value = type.getConstructor().newInstance();
						beanDesc = BeanEditor.registerBean(value, beanName, this.local);
						if (!new BeanManager(value, this.local).load(new File(this.local + File.separator + beanFileName))) {
							BeanEditor.beanInstances.get(type).remove(beanDesc);
							value = null;
						}
					} else
						value = beanDesc.bean;
				}
			}
			if (this.subClasses != null && needToLoadBrotherBean) {
				String[] baseNames = new String[this.subClasses.length];
				int i = 0;
				for (Class<?> c : this.subClasses)
					baseNames[i++] = BeanManager.getDescriptorFromClassWithSimpleName(c);
				if (this.local != null) {
					File[] files = new File(this.local).listFiles();
					if (files != null)
						for (File file : files) {
							String fileName = file.getName();
							int indexofti = fileName.indexOf(BeanDesc.SEPARATOR);
							if (indexofti == -1)
								continue;
							String baseFileName = fileName.substring(0, indexofti);
							for (String baseName : baseNames)
								if (baseFileName.equals(baseName) && (beanFileName == null || !beanFileName.equals(fileName))) {
									Class<?> type = getType(fileName.substring(0, indexofti));
									if (type == null)
										continue;
									String brotherName = fileName.substring(baseName.length() + 1);
									brotherName = brotherName.substring(0, brotherName.lastIndexOf("."));
									if (BeanEditor.getRegisterBean(type, brotherName) == null) {
										Object brotherBean = type.getConstructor().newInstance();
										BeanDesc<?> beanDesc = BeanEditor.registerBean(brotherBean, brotherName, this.local);
										if (!new BeanManager(brotherBean, this.local).load(file))
											BeanEditor.beanInstances.get(type).remove(beanDesc);
									}
									break;
								}
						}
				}
			}
			setValueFromObj(value);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			setValue(null);
		}
	}

	public void setSubClasses(Class<?>[] subClasses) {
		if (subClasses != null && subClasses.length != 0) {
			LinkedHashSet<Class<?>> subClassesSet = new LinkedHashSet<>();
			for (Class<?> class1 : subClasses) {
				boolean isAssignable = getGenericComponentType().isAssignableFrom(class1);
				boolean hadPublicDefaultConstructor = PropertyEditorManager.hadPublicDefaultConstructor(class1);
				boolean isNotAbstract = !Modifier.isAbstract(class1.getModifiers());
				if (isAssignable && hadPublicDefaultConstructor && isNotAbstract) // Sinon je ne peux jamais avoir le père
					subClassesSet.add(class1);
				else if (!isAssignable)
					Log.error("ObjectEditor error, the class type choice: " + class1 + " is not assignable to " + getGenericComponentType());
				else if (!hadPublicDefaultConstructor)
					Log.error("ObjectEditor error, the class type choice: " + class1 + "for the class: " + getGenericComponentType() + " don't have public constructor");
				else if (!isNotAbstract)
					Log.error("ObjectEditor error, the class type choice: " + class1 + "for the class: " + getGenericComponentType() + " is abstract");
			}
			int i = 0;
			int subClassesSize = subClassesSet.size();
			if (subClassesSize != 0) {
				this.subClasses = new Class<?>[subClassesSize];
				for (Class<?> subClass : subClassesSet)
					this.subClasses[i++] = subClass;
				return;
			}
		}
		if (!Modifier.isAbstract(getComponentType().getModifiers()))
			this.subClasses = new Class<?>[] { getComponentType() };
		else
			throw new IllegalArgumentException("BeanEditor with an abstract type must have valid non-null sub-classes");
	}

	public Class<?>[] getSubClasses() {
		return this.subClasses;
	}

	@Override
	public void setValue(T value) {
		if (value != null) {
			Class<?> valueClass = value.getClass();
			if (this.subClasses == null)
				value = null;
			else {
				boolean isInChoice = false;
				for (int i = 0; i < this.subClasses.length; i++)
					if (BeanManager.getDescriptorFromClass(this.subClasses[i]).equals(BeanManager.getDescriptorFromClass(valueClass))) {
						isInChoice = true;
						break;
					}
				if (!isInChoice)
					value = null;
			}
		}
		super.setValue(value);
	}

	private void setNewValueFromSubClassesPossibilities(Window window) {
		Stage dialog = new Stage(StageStyle.UTILITY);
		dialog.initModality(Modality.WINDOW_MODAL);
		dialog.initOwner(window);
		dialog.setTitle("Type Choice");
		ListView<Class<?>> listView = new ListView<>(FXCollections.observableList(Arrays.asList(this.subClasses)));
		listView.setCellFactory(e -> {
			return new ListCell<>() {
				@Override
				protected void updateItem(Class<?> c, boolean empty) {
					super.updateItem(c, empty);
					setText(c == null || empty ? null : c.getSimpleName());
				}
			};
		});
		listView.setOnMouseClicked(e -> {
			if (e.getClickCount() == 2) {
				Class<?> type = listView.getSelectionModel().getSelectedItem();
				try {
					Object bean = type.getConstructor().newInstance();
					registerCreatedBeanAndCreateSubBean(bean, this.local);
					setValueFromObj(bean);
					fireBeanListChange();
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
					ex.printStackTrace();
				}
				dialog.close();
			}
		});
		listView.setFixedCellSize(24);
		listView.setPrefHeight(listView.getFixedCellSize() * this.subClasses.length + 2);
		Scene scene = new Scene(new Group(listView));
		dialog.setScene(scene);
		dialog.setOnCloseRequest(e -> updateCustomEditor());
		dialog.show();
	}

	@Override
	public void updateCustomEditor() {
		HBox hBox = (HBox) customEditor();
		if (hBox == null)
			return;
		updateCustomEditor(hBox);
	}

	private void updateCustomEditor(HBox hBox) {
		if (this.inline || getValue() != null && getBeanDesc(getValue()) == null) {
			if (this.inline && hBox.getChildren().size() != 1 || !this.inline && hBox.getChildren().size() != 2) {
				populateEditor(hBox);
				return;
			}
		} else if (this.comboBox == null) {
			populateEditor(hBox);
			return;
		}
		if (this.comboBox != null) {
			Object newVal = getValue() != null ? getBeanDesc(getValue()) : String.valueOf((Object) null);
			if (this.comboBox.getValue() != newVal)
				this.comboBox.setValue(newVal);
		} else if (this.objectLabel != null)
			this.objectLabel.setText(getValue() == null ? String.valueOf((Object) null) : getValue().toString());
	}

	private void updateModel(HBox hBox) {
		LinkedList<BeanDesc<?>> subBeans = new LinkedList<>();
		if (this.subClasses != null)
			for (Class<?> c : this.subClasses) {
				BeanList<?> sb = beanInstances.get(c);
				if (sb != null)
					subBeans.addAll(sb);
			}
		else {
			Object value = getValue();
			if (value instanceof BeanDesc)
				subBeans.add((BeanDesc<?>) value);
		}
		// Si pas de windows, pas la peine de runlater
		if (this.comboBox != null) {
			ObservableList<Object> items = this.comboBox.getItems();
			items.clear();
			items.addAll(subBeans.toArray());
			if (nullable())
				items.add(NULL);
			if (this.subClasses != null)
				items.add(NEWINSTANCE);
			this.comboBox.setVisibleRowCount(Math.min(items.size(), 10));
			updateCustomEditor(hBox);
		}
	}

	private void updateComboboxValue() {
		Object value = this.comboBox.getValue();
		if (value instanceof String) {
			if (value.equals(NEWINSTANCE)) {
				if (this.subClasses.length > 1)
					setNewValueFromSubClassesPossibilities(this.comboBox.getScene().getWindow());
				else
					Platform.runLater(() -> { // Bug Fx modif quand dans selection model changing
						setNewValueFromDefaultObject();
					});
			} else {
				Object val = getValue();
				if (val != null) {
					setValue(null);
					fireBeanListChange();
				}
			}
		} else {
			BeanDesc<?> newBeanDesc = (BeanDesc<?>) value;
			Object oldObject = getValue();
			if (oldObject != newBeanDesc.bean) {
				setValueFromObj(newBeanDesc.bean);
				fireBeanListChange();
			}
		}
	}

	private void setNewValueFromDefaultObject() {
		try {
			BeanDesc<?> beanDesc = registerCreatedBeanAndCreateSubBean(createDefaultObject(), this.local);
			setValueFromObj(beanDesc.bean);
			updateCustomEditor();
			fireBeanListChange();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public void addBeanListChangedListener(BeanListChangeListener listener) {
		this.beansListlisteners.addStrongRef(listener);
	}

	public void removeBeanListChangedListener(BeanListChangeListener listener) {
		this.beansListlisteners.removeStrongRef(listener);
	}

	private void fireBeanListChange() {
		this.beansListlisteners.forEach(l -> l.beanListChanged());
	}

	@Override
	public void writeValue(DataOutput raf, Object value) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			new BeanManager(value, "").save(baos, "");
			byte[] byteArray = baos.toByteArray();
			raf.writeUTF(BeanManager.getDescriptorFromClass(value.getClass()));
			raf.writeInt(byteArray.length);
			raf.write(byteArray);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public String getDescription() {
		return this.inline ? "inline" : "shared";
	}

	/** Invoked when a bean is created. A strong reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanCreatedListener(BeanCreatedListener listener) { // Utilisé pour créer le fichier par le beanManager
		BEANS_CREATED_LISTENERS.addStrongRef(listener);
	}

	/** Invoked when a bean is created. A strong reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the function
	 * is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanCreatedListenerIfAbsent(BeanCreatedListener listener) {
		BEANS_CREATED_LISTENERS.addStrongRefIfAbsent(listener);
	}

	/** Invoked when a bean is created. A weak reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanCreatedListener(BeanCreatedListener listener) {
		BEANS_CREATED_LISTENERS.addWeakRef(listener);
	}

	/** Invoked when a bean is created. A weak reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the function
	 * is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanCreatedListenerIfAbsent(BeanCreatedListener listener) {
		BEANS_CREATED_LISTENERS.addWeakRefIfAbsent(listener);
	}

	/** Remove the strong reference to a function to be called when a bean is created.
	 * @param listener The listener to remove */
	public static void removeStrongRefBeanCreatedListener(BeanCreatedListener listener) {
		BEANS_CREATED_LISTENERS.removeStrongRef(listener);
	}

	/** Remove the weak reference to a function to be called when a bean is created.
	 * @param listener The listener to remove */
	public static void removeWeakRefBeanCreatedListener(BeanCreatedListener listener) {
		BEANS_CREATED_LISTENERS.removeWeakRef(listener);
	}

	/** Fire bean created event.
	 * @param beanDesc The descriptor of the created bean. */
	private static void fireBeanCreatedListener(BeanDesc<?> beanDesc) {
		BEANS_CREATED_LISTENERS.forEach(l -> l.beanCreated(beanDesc));
	}

	/** Invoked when a bean is registered. A strong reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanRegisterListener(BeanRegisterListener listener) {
		BEANS_REGISTERED_LISTENERS.addStrongRef(listener);
	}

	/** Invoked when a bean is registered. A strong reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the
	 * function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanRegisterListenerIfAbsent(BeanRegisterListener listener) {
		BEANS_REGISTERED_LISTENERS.addStrongRefIfAbsent(listener);
	}

	/** Invoked when a bean is registered. A weak reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanRegisterListener(BeanRegisterListener listener) {
		BEANS_REGISTERED_LISTENERS.addWeakRef(listener);
	}

	/** Invoked when a bean is registered. A weak reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the
	 * function is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanRegisterListenerIfAbsent(BeanRegisterListener listener) {
		BEANS_REGISTERED_LISTENERS.addWeakRefIfAbsent(listener);
	}

	/** Remove the strong reference to a function to be called when a bean is registered.
	 * @param listener The listener to remove */
	public static void removeStrongRefBeanRegisterListener(BeanRegisterListener listener) {
		BEANS_REGISTERED_LISTENERS.removeStrongRef(listener);
	}

	/** Remove the weak reference to a function to be called when a bean is registered.
	 * @param listener The listener to remove */
	public static void removeWeakRefBeanRegisterListener(BeanRegisterListener listener) {
		BEANS_REGISTERED_LISTENERS.removeWeakRef(listener);
	}

	/** Fire bean registered event.
	 * @param beanDesc The descriptor of the register bean. */
	private static void fireBeanRegister(BeanDesc<?> beanDesc) {
		BEANS_REGISTERED_LISTENERS.forEach(l -> l.beanRegister(beanDesc));
	}

	/** Invoked when a bean is renamed. A strong reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanRenameListener(BeanRenameListener listener) {
		BEANS_RENAMED_LISTENERS.addStrongRef(listener);
	}

	/** Invoked when a bean is renamed. A strong reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the function
	 * is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanRenameListenerIfAbsent(BeanRenameListener listener) {
		BEANS_RENAMED_LISTENERS.addStrongRefIfAbsent(listener);
	}

	/** Invoked when a bean is renamed. A weak reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanRenameListener(BeanRenameListener listener) {
		BEANS_RENAMED_LISTENERS.addWeakRef(listener);
	}

	/** Invoked when a bean is renamed. A weak reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the function
	 * is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanRenameListenerIfAbsent(BeanRenameListener listener) {
		BEANS_RENAMED_LISTENERS.addWeakRefIfAbsent(listener);
	}

	/** Remove the strong reference to a function to be called when a bean is renamed.
	 * @param listener The listener to remove */
	public static void removeStrongRefBeanRenameListener(BeanRenameListener listener) {
		BEANS_RENAMED_LISTENERS.removeStrongRef(listener);
	}

	/** Remove the weak reference to a function to be called when a bean is renamed.
	 * @param listener The listener to remove */
	public static void removeWeakRefBeanRenameListener(BeanRenameListener listener) {
		BEANS_RENAMED_LISTENERS.removeWeakRef(listener);
	}

	/** Fire bean renamed event.
	 * @param oldBeanDesc The descriptor of the old register bean.
	 * @param beanDesc The descriptor of the new register bean. */
	private static void fireBeanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		BEANS_RENAMED_LISTENERS.forEach(l -> l.beanRename(oldBeanDesc, beanDesc));
	}

	/** Invoked when a bean is unregistered. A strong reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanUnregisterListener(BeanUnregisterListener listener) {
		BEANS_UNREGISTERED_LISTENERS.addStrongRef(listener);
	}

	/** Invoked when a bean is unregistered. A strong reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the
	 * function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefBeanUnregisterListenerIfAbsent(BeanUnregisterListener listener) {
		BEANS_UNREGISTERED_LISTENERS.addStrongRefIfAbsent(listener);
	}

	/** Invoked when a bean is unregistered. A weak reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanUnregisterListener(BeanUnregisterListener listener) {
		BEANS_UNREGISTERED_LISTENERS.addWeakRef(listener);
	}

	/** Invoked when a bean is unregistered. A weak reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the
	 * function is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefBeanUnregisterListenerIfAbsent(BeanUnregisterListener listener) {
		BEANS_UNREGISTERED_LISTENERS.addWeakRefIfAbsent(listener);
	}

	/** Remove the strong reference to a function to be called when a bean is unregistered.
	 * @param listener The listener to remove */
	public static void removeStrongRefBeanUnregisterListener(BeanUnregisterListener listener) {
		BEANS_UNREGISTERED_LISTENERS.removeStrongRef(listener);
	}

	/** Remove the weak reference to a function to be called when a bean is unregistered.
	 * @param listener The listener to remove */
	public static void removeWeakRefBeanUnregisterListener(BeanUnregisterListener listener) {
		BEANS_UNREGISTERED_LISTENERS.removeStrongRef(listener);
	}

	/** Fire bean unregistered event.
	 * @param beanDesc The descriptor of the unregistered bean.
	 * @param delete true if the node need to be deleted */
	private static void fireBeanUnregister(BeanDesc<?> beanDesc, boolean delete) {
		BEANS_UNREGISTERED_LISTENERS.forEach(l -> l.beanUnregister(beanDesc, delete));
	}

	public static List<BeanDesc<?>> getAllBeans() {
		ArrayList<BeanDesc<?>> beans = new ArrayList<>();
		for (BeanList<?> beanDescs : beanInstances.values())
			beans.addAll(beanDescs);
		return beans;
	}

	public static <T> BeanDesc<T> getBeanDesc(T bean) {
		@SuppressWarnings("unchecked")
		List<BeanDesc<T>> beanDescs = beanInstances.get((Class<T>) bean.getClass());
		if (beanDescs == null)
			return null;
		for (BeanDesc<T> beanDesc : beanDescs)
			if (beanDesc.bean == bean)
				return beanDesc;
		return null;
	}

	public static <T> BeanDesc<T> getBeanDescWithAlias(String beanInfo) throws ClassNotFoundException {
		int i = beanInfo.indexOf(BeanDesc.SEPARATOR);
		@SuppressWarnings("unchecked")
		List<BeanDesc<T>> beans = beanInstances.get((Class<T>) BeanManager.getClassFromDescriptor(beanInfo.substring(0, i)));
		if (beans != null) {
			String beanName = beanInfo.substring(i + 1);
			for (BeanDesc<T> beanDesc : beans)
				if (beanDesc.name.equals(beanName))
					return beanDesc;
		}
		return null;
	}

	public static <T> List<BeanDesc<T>> getBeanInstance(Class<T> type) {
		return beanInstances.get(type);
	}

	public static <T> String getNextDefaultBeanName(Class<T> type) {
		int id = 0;
		List<BeanDesc<T>> brothersBeans = beanInstances.get(type);
		String baseName = type.getSimpleName() + BeanDesc.SEPARATOR;
		if (brothersBeans != null) {
			String name;
			boolean isValidName;
			do {
				isValidName = true;
				name = baseName + id++;
				for (BeanDesc<?> brotherBeanDesc : brothersBeans)
					if (brotherBeanDesc.name.equals(name)) {
						isValidName = false;
						break;
					}
			} while (!isValidName || id == Integer.MAX_VALUE);
			return name;
		}
		return baseName + id;
	}

	public static <T> BeanDesc<T> getRegisterBean(Class<T> type, String name) {
		List<BeanDesc<T>> beans = beanInstances.get(type);
		if (beans != null)
			for (BeanDesc<T> beanDesc : beans)
				if (beanDesc.name.equals(name))
					return beanDesc;
		return null;
	}

	private static boolean isValidBeanName(String name) {
		return name != null && !name.isEmpty() && !Pattern.compile("[/\\\\?%*:|\\\"<>]").matcher(name).find() && !name.startsWith(".") && !name.trim().isEmpty();
	}

	public static void main(String[] args) {
		FxTest.launchIHM(args,
				s -> new VBox(new BeanEditor<>(TypeTest.class, new File("").getAbsolutePath()).getCustomEditor(), new BeanEditor<>(TypeTest.class, new File("").getAbsolutePath()).getCustomEditor()));
	}

	public static <T> BeanDesc<T> registerBean(T bean, String local) {
		return registerBean(bean, BeanEditor.getNextDefaultBeanName(bean.getClass()), local);
	}

	public static <T> BeanDesc<T> registerBean(T bean, String name, String local) {
		if (bean == null)
			throw new IllegalArgumentException("bean must not be null");
		if (!isValidBeanName(name))
			throw new IllegalArgumentException("Invalid bean name: " + name);
		@SuppressWarnings("unchecked")
		Class<T> type = (Class<T>) bean.getClass();
		BeanList<T> subBeans = beanInstances.get(type);
		if (subBeans == null) {
			subBeans = new BeanList<>();
			beanInstances.put(type, subBeans);
		}
		for (BeanDesc<T> beanDesc : subBeans)
			if (beanDesc.name.equals(name))
				return beanDesc;
		BeanDesc<T> beanDesc = new BeanDesc<>(bean, name, local);
		subBeans.add(beanDesc);
		fireBeanRegister(beanDesc);
		return beanDesc;
	}

	public static <T> BeanDesc<T> registerCreatedBeanAndCreateSubBean(T bean, String local) {
		return registerCreatedBeanAndCreateSubBean(bean, BeanEditor.getNextDefaultBeanName(bean.getClass()), local);
	}

	public static <T> BeanDesc<T> registerCreatedBeanAndCreateSubBean(T bean, String name, String local) {
		BeanDesc<T> beanDesc = BeanEditor.registerBean(bean, name, local);
		BeanManager.createSubBeans(bean, local);
		fireBeanCreatedListener(beanDesc);
		return beanDesc;
	}

	public static <T> void renameBean(BeanDesc<T> beanDesc, String newBeanName) {
		// Log.info("rename bean: " + beanDesc + " to: " + newBeanName);
		T bean = beanDesc.bean;
		if (beanDesc.name.equals(newBeanName))
			return;
		if (!isValidBeanName(newBeanName))
			throw new IllegalArgumentException("The new bean name is not a valid bean name");
		@SuppressWarnings("unchecked")
		List<BeanDesc<T>> brotherBeanDescs = beanInstances.get((Class<T>) bean.getClass());
		for (BeanDesc<T> brotherBeanDesc : brotherBeanDescs)
			if (brotherBeanDesc.name.equals(newBeanName))
				throw new IllegalArgumentException("The new bean name is not valid, an other operator have the same name");
		if (!brotherBeanDescs.remove(beanDesc))
			throw new IllegalArgumentException("The bean to rename is not registered in ObjectEditor");
		BeanDesc<T> newBeanDesc = new BeanDesc<>(beanDesc.bean, newBeanName, beanDesc.local);
		brotherBeanDescs.add(newBeanDesc);
		fireBeanRename(beanDesc, newBeanDesc);
	}

	public static void setIconeProvider(Function<Object, Image> iconProvider) {
		BeanEditor.iconProvider = iconProvider;
	}

	public static <T> void unregisterBean(BeanDesc<T> beanDesc) {
		Class<?> type = beanDesc.bean.getClass();
		BeanList<?> subBeans = beanInstances.get(type);
		if (subBeans == null)
			return;
		subBeans.remove(beanDesc);
		if (subBeans.isEmpty())
			beanInstances.remove(type);
	}

	public static <T> void unregisterBean(Object bean, boolean delete) {
		BeanList<?> subBeans = beanInstances.get(bean.getClass());
		if (subBeans == null)
			return;
		for (BeanDesc<?> beanDesc : subBeans)
			if (beanDesc.bean == bean) { // pas de equals, sinon pb avec localframe avec les mêmes données
				subBeans.remove(beanDesc);
				if (subBeans.size() == 0)
					beanInstances.remove(bean.getClass());
				fireBeanUnregister(beanDesc, delete);
				return;
			}
	}

	@Override
	public void loaded(Module module) {}

	@Override
	public Runnable modified(Module module) {
		return null;
	}

	@Override
	public void unloaded(Module moduleToRemove) {
		if (this.subClasses != null) {
			ArrayList<Class<?>> subclassesToRemove = null;
			for (Class<?> subClass : this.subClasses) {
				Module module = subClass.getModule();
				if (module != null && module.equals(moduleToRemove)) {
					if (subclassesToRemove == null)
						subclassesToRemove = new ArrayList<>();
					subclassesToRemove.add(subClass);
				}
			}
			if (subclassesToRemove != null) {
				List<Class<?>> newSubClasses = new ArrayList<>(Arrays.asList(this.subClasses));
				newSubClasses.removeAll(subclassesToRemove);
				setSubClasses(newSubClasses.toArray(Class[]::new));
			}
		}
	}
}

class BeanInstancesMap {
	private final HashMap<Class<?>, BeanList<?>> map = new HashMap<>();

	@SuppressWarnings("unchecked")
	public <T, U extends BeanList<T>> BeanList<T> get(Class<T> key) {
		return (BeanList<T>) this.map.get(key);
	}

	public <T> void put(Class<T> key, BeanList<T> editor) {
		this.map.put(key, editor);
	}

	public void remove(Class<?> key) {
		this.map.remove(key);
	}

	public Collection<BeanList<?>> values() {
		return this.map.values();
	}
}

class BeanList<T> extends ArrayList<BeanDesc<T>> {
	private static final long serialVersionUID = 1L;

}

class ObjectListcell extends ListCell<Object> {
	private static final Image BEAN_ICON = new Image(LoadPackageStream.getStream("/bean.gif"));

	@Override
	protected void updateItem(Object item, boolean empty) {
		super.updateItem(item, empty);
		setText(item == null || empty ? null : item.toString());
		if (item == null || !(item instanceof BeanDesc))
			setGraphic(null);
		else {
			Image image;
			setGraphic(new ImageView(BeanEditor.iconProvider != null && (image = BeanEditor.iconProvider.apply(((BeanDesc<?>) item).bean)) != null ? image : BEAN_ICON));
		}
	}
}
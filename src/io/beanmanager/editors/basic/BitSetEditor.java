/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.basic;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.BitSet;
import java.util.Objects;

import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.ihmtest.FxTest;
import io.beanmanager.internal.Log;
import io.beanmanager.tools.FxUtils;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

public class BitSetEditor extends PropertyEditor<BitSet> {
	private static final int BITS_IN_ONE_CHAR_VALUE = 4;
	private static final int HEX_RADIX = 16;
	private int minSize = 0;
	private int maxSize = Integer.MAX_VALUE;
	private ContextMenu popupMenu;
	private CheckMenuItem binView;
	private CheckMenuItem hexView;
	private boolean isBinView;

	public BitSetEditor() {}

	public BitSetEditor(int minSize, int maxSize) {
		this.minSize = minSize;
		this.maxSize = maxSize;
	}

	public static void main(String[] args) {
		BitSet bs = new BitSet();
		bs.set(9);
		bs.set(8);
		bs.set(3);

		Log.info(bitSetToBitString(bs));

		Log.info(bitSetToHexString(bs));

		BitSetEditor bse = new BitSetEditor(8, 24);
		bse.setValue(bs);
		BitSet bs2 = bse.getValue();
		Log.info(bs2.toString());
		Log.info(bitStringToBitSet(bitSetToBitString(bs)).toString());
		Log.info(hexStringToBitSet(bitSetToHexString(bs)).toString());

		FxTest.launchIHM(args, s -> new VBox(bse.getEditor(), new BitSetEditor().getEditor()));
	}

	@Override
	protected TextField getCustomEditor() {
		this.popupMenu = new ContextMenu();
		this.binView = new CheckMenuItem("Binary view");
		this.hexView = new CheckMenuItem("Hexadecimal view");
		if (isHexViewAble()) {
			this.hexView.setSelected(true);
			this.binView.setSelected(false);
			this.isBinView = false;
		} else {
			this.hexView.setSelected(false);
			this.binView.setSelected(true);
			this.isBinView = true;
		}
		this.popupMenu.getItems().addAll(this.binView, this.hexView);
		TextField textField = new TextField(this.isBinView ? bitSetToSizedBitString(getValue()) : bitSetToHexString(getValue()));
		textField.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				updateValue();
				FxUtils.traverse();
				e.consume();
			}
		});
		textField.textProperty().addListener(e -> updateStyle());
		textField.focusedProperty().addListener((ov, oldValue, newValue) -> {
			if (!newValue)
				updateValue();
		});
		textField.setPrefWidth(150);
		textField.setContextMenu(this.popupMenu);

		EventHandler<ActionEvent> eh = e -> {
			Object src = e.getSource();
			if (src == this.binView) {
				updateValue();
				this.binView.setSelected(true);
				this.hexView.setSelected(false);
				this.isBinView = true;
				updateCustomEditor();
			} else {
				updateValue();
				this.binView.setSelected(false);
				this.hexView.setSelected(true);
				this.isBinView = false;
				updateCustomEditor();
			}
		};
		this.binView.setOnAction(eh);
		this.hexView.setOnAction(eh);
		return textField;
	}

	private void updateStyle() {
		TextField textField = (TextField) customEditor();
		String text = textField.getText().replaceAll("\\s", "");
		textField.setStyle("-fx-text-fill: " + (!isValidSelection(text) ? "red"
				: text.equals(this.isBinView ? bitSetToSizedBitString(getValue()).replaceAll("\\s", "") : bitSetToHexString(getValue()).replaceAll("\\s", "")) ? "black" : "green"));
	}

	@Override
	public void setValue(BitSet value) {
		if (value == null || value.length() >= this.minSize && value.length() <= this.maxSize)
			super.setValue(value);
	}

	@Override
	public int getPitch() {
		return -1;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	private boolean isHexViewAble() {
		return this.minSize % 8 == 0 && this.maxSize % 8 == 0;
	}

	private boolean isValidSelection(String text) {
		text = text.replaceAll("\\s", "");
		return text.isEmpty() ? true
				: this.hexView.isSelected() ? text.matches("[0-9A-F]+") && text.length() >= this.minSize / 4 && text.length() <= this.maxSize / 4 && text.length() % 2 == 0
						: text.matches("[0-1]+") && text.length() >= this.minSize && text.length() <= this.maxSize;
	}

	@Override
	public BitSet readValue(DataInput raf) throws IOException {
		byte[] bitArray = new byte[raf.readInt()];
		raf.readFully(bitArray);
		return BitSet.valueOf(bitArray);
	}

	@Override
	public void writeValue(DataOutput raf, BitSet bitSet) throws IOException {
		byte[] ba = bitSet.toByteArray();
		raf.writeInt(ba.length);
		raf.write(ba);
	}

	@Override
	public String getAsText() {
		BitSet value = getValue();
		return value != null ? bitSetToSizedBitString(value) : null;
	}

	@Override
	public void setAsText(String text) {
		setValue(text.equals(String.valueOf((Object) null)) ? null : bitStringToBitSet(text));
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
		BitSet bitSet = getValue();
		if (bitSet != null && bitSet.length() >= maxSize) {
			BitSet newBitSet = new BitSet(maxSize);
			for (int i = 0; i < newBitSet.length(); i++)
				if (bitSet.get(i))
					newBitSet.set(i);
				else
					newBitSet.clear(i);
			setValue(newBitSet);
		}
	}

	public void setMinSize(int minSize) {
		if (minSize < 0)
			minSize = 0;
		this.minSize = minSize;
		BitSet bitSet = getValue();
		if (bitSet != null && bitSet.length() < minSize)
			for (int i = bitSet.length(); i < minSize; i++)
				bitSet.clear(i);
		if (this.hexView != null && this.hexView.isSelected() && !isHexViewAble()) {
			this.binView.setSelected(true);
			this.hexView.setSelected(false);
		}
	}

	@Override
	public void updateCustomEditor() {
		TextField textField = (TextField) customEditor();
		String text = this.isBinView ? bitSetToSizedBitString(getValue()) : bitSetToHexString(getValue());
		if (!text.equals(textField.getText()))
			textField.setText(text);
		updateStyle();
	}

	public void updateValue() {
		TextField textField = (TextField) customEditor();
		String text = textField.getText();
		if (isValidSelection(text)) {
			BitSet value = this.isBinView ? bitStringToBitSet(text) : hexStringToBitSet(text);
			if (Objects.equals(getValue(), value))
				setValue(value);
		} else
			textField.setText(this.isBinView ? bitSetToSizedBitString(getValue()) : bitSetToHexString(getValue()));
		updateGUI();
	}

	private String bitSetToSizedBitString(BitSet bitSet) {
		String text = bitSetToBitString(bitSet);
		int oriSize = text.replaceAll("\\s", "").length();
		if (oriSize < this.minSize) {
			int index = oriSize % 8;
			for (int i = 0; i <= this.minSize - oriSize - 1; i++) {
				if (index++ == 8) {
					text = " " + text;
					index = 0;
				}
				text = "0" + text;
			}
		}
		oriSize = text.replaceAll("\\s", "").length();
		if (oriSize > this.maxSize)
			text = text.substring(0, this.maxSize);
		return text;
	}

	public static String bitSetToBitString(BitSet bitSet) {
		if (bitSet == null)
			return "";
		if (bitSet.isEmpty())
			return "0";
		StringBuffer bitSetString = new StringBuffer();
		for (int i = bitSet.length(); i-- != 0;) {
			bitSetString.append(bitSet.get(i) ? "1" : "0");
			if (i != 0 && i % 8 == 0)
				bitSetString.append(" ");
		}
		return bitSetString.toString();
	}

	public static BitSet bitStringToBitSet(String bitSetString) {
		bitSetString = bitSetString.replaceAll("\\s", "");
		if (bitSetString == null || bitSetString.isEmpty())
			return null;
		BitSet bitSet = new BitSet();
		int bitSetIndex = 0;
		for (int i = bitSetString.length(); i-- != 0;) {
			if (bitSetString.charAt(i) == '1')
				bitSet.set(bitSetIndex);
			bitSetIndex++;
		}
		return bitSet;
	}

	public static char bitSetToHexadecimalChar(final BitSet bitSet) {
		int hexChar = 0;
		for (int i = 0; i < BITS_IN_ONE_CHAR_VALUE; i++)
			if (bitSet.get(i))
				hexChar |= 1 << i;
		return Character.forDigit(hexChar, HEX_RADIX);
	}

	public static String bitSetToHexadecimalString(final BitSet bitSet) {
		if (bitSet.isEmpty())
			return "0";
		StringBuffer hexStringBuffer = new StringBuffer();
		for (int i = (int) Math.ceil(bitSet.length() / 8.0); i-- != 0;) {
			int j = i * 8;
			hexStringBuffer.append(Character.toUpperCase(bitSetToHexadecimalChar(bitSet.get(j + 4, j + 8))));
			hexStringBuffer.append(Character.toUpperCase(bitSetToHexadecimalChar(bitSet.get(j, j + 4))));
			if (i != 0)
				hexStringBuffer.append(" ");
		}
		return hexStringBuffer.toString();
	}

	private static String bitSetToHexString(BitSet bitSet) {
		return bitSet == null ? "" : bitSet.length() == 0 ? "00" : bitSetToHexadecimalString(bitSet);
	}

	public static BitSet hexCharToBitSet(final Character hexChar) {
		final BitSet charBitSet = new BitSet();
		final int hex = Integer.parseInt(hexChar.toString(), HEX_RADIX);
		for (int i = 0; i < BITS_IN_ONE_CHAR_VALUE; i++)
			if (Integer.lowestOneBit(hex >> i) == 1)
				charBitSet.set(i);
		return charBitSet;
	}

	public static BitSet hexStringToBitSet(String hex) {
		hex = hex.replaceAll("\\s", "");
		if (hex.isEmpty())
			return null;
		final int hexBitSize = hex.length() * BITS_IN_ONE_CHAR_VALUE;
		final BitSet hexBitSet = new BitSet(hexBitSize);
		int index = 0;
		for (int i = hex.length(); i-- != 0;) {
			final int hexChar = Integer.parseInt(String.valueOf(hex.charAt(i)), HEX_RADIX);
			for (int j = 0; j < BITS_IN_ONE_CHAR_VALUE; j++)
				if (Integer.lowestOneBit(hexChar >> j) == 1)
					hexBitSet.set(index + j);
			index += 4;
		}
		return hexBitSet;
	}

	@Override
	public String getDescription() {
		return this.minSize == 0 && this.maxSize == Integer.MAX_VALUE ? null
				: "Size: " + (this.minSize == 0 ? "" : "min: " + this.minSize) + (this.maxSize == Integer.MAX_VALUE ? "" : "max: " + this.maxSize);
	}
}

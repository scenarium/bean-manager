/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.basic;

import java.io.File;

import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class FileEditor extends AbstractPathEditor<File> {

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new FileEditor().getCustomEditor(), new FileEditor(true).getCustomEditor()));
	}

	public FileEditor() {}

	public FileEditor(boolean isDirectory) {
		super(isDirectory);
	}

	@Override
	public String getAsText() {
		File value = getValue();
		return value != null ? value.getPath() : null;
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null ? null : new File(text));
	}

	@Override
	protected File getValueAsFile() {
		return getValue();
	}

	@Override
	protected void setValueAsFile(File value) {
		setValue(value);
	}
}

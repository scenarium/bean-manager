/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors;

import java.lang.annotation.Annotation;
import java.util.function.Function;

import io.beanmanager.tools.TriConsumer;

public class EditorDescriptor<T, U extends PropertyEditor<T>> {
	public final Class<U> editorClass;
	private final ClassToTriConsumerMap<U, Function<String, Object>> annotationClasses = new ClassToTriConsumerMap<>();
	private final ClassToTriConsumerMap<U, Function<String, Object>> dynamicAnnotationClasses = new ClassToTriConsumerMap<>();

	public static <T, U extends PropertyEditor<T>> EditorDescriptor<T, U> build(Class<U> editorClass) {
		return new EditorDescriptor<>(editorClass);
	}

	private EditorDescriptor(Class<U> editorClass) {
		this.editorClass = editorClass;
	}

	public <V> EditorDescriptor<T, U> addAnnotation(Class<? extends V> annotation, TriConsumer<U, V, Function<String, Object>> annotationFunction) {
		this.annotationClasses.put(annotation, annotationFunction);
		return this;
	}

	public <V> EditorDescriptor<T, U> addDynamicAnnotation(Class<? extends V> annotation, TriConsumer<U, V, Function<String, Object>> annotationFunction) {
		this.dynamicAnnotationClasses.put(annotation, annotationFunction);
		return this;
	}

	void apply(PropertyEditor<?> propertyEditor, Annotation[] anno, Function<String, Object> methodInvocator) {
		U editor = this.editorClass.cast(propertyEditor);
		for (Annotation annotation : anno)
			consume(editor, annotation, annotation.annotationType(), methodInvocator);
	}

	private <W> void consume(U editor, Object annotation, Class<W> c, Function<String, Object> methodInvocator) {
		TriConsumer<U, ? super W, Function<String, Object>> triConsumer = this.annotationClasses.get(c);
		if (triConsumer == null)
			triConsumer = this.dynamicAnnotationClasses.get(c);
		if (triConsumer != null)
			triConsumer.accept(editor, c.cast(annotation), methodInvocator);
	}

	void applyDynamic(PropertyEditor<?> propertyEditor, Annotation[] anno, Function<String, Object> methodInvocator) {
		U editor = this.editorClass.cast(propertyEditor);
		for (Annotation annotation : anno)
			consumeDynamic(editor, annotation, annotation.annotationType(), methodInvocator);
	}

	private <W> void consumeDynamic(U editor, Object annotation, Class<W> c, Function<String, Object> methodInvocator) {
		TriConsumer<U, ? super W, Function<String, Object>> triConsumer = this.dynamicAnnotationClasses.get(c);
		if (triConsumer != null)
			triConsumer.accept(editor, c.cast(annotation), methodInvocator);
	}
}

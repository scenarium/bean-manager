/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors;

import java.util.WeakHashMap;

import javax.swing.event.EventListenerList;

import io.beanmanager.tools.FxUtils;

public interface DynamicEnableBean {
	static WeakHashMap<Object, EventListenerList> LISTENERS = new WeakHashMap<>();

	public static void addEnableChangeListener(Object bean, DynamicEnableBeanPropertyChangeListener listener) {
		EventListenerList list = LISTENERS.get(bean);
		if (list == null) {
			list = new EventListenerList();
			LISTENERS.put(bean, list);
		}
		list.add(DynamicEnableBeanPropertyChangeListener.class, listener);
	}

	public static void removeEnableChangeListener(Object bean, DynamicEnableBeanPropertyChangeListener listener) {
		EventListenerList list = LISTENERS.get(bean);
		if (list == null)
			return;
		list.remove(DynamicEnableBeanPropertyChangeListener.class, listener);
	}

	default void fireSetPropertyEnable(DynamicEnableBean bean, String propertyName, boolean enable) {
		FxUtils.runLaterIfNeeded(() -> {
			EventListenerList list = LISTENERS.get(bean);
			if (list == null)
				return;
			for (DynamicEnableBeanPropertyChangeListener listener : list.getListeners(DynamicEnableBeanPropertyChangeListener.class))
				listener.enablePropertyChange(propertyName, enable);
		});
	}

	public void setEnable();
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors;

import java.util.WeakHashMap;

import javax.swing.event.EventListenerList;

import io.beanmanager.tools.FxUtils;

public interface UpdatableViewBean {
	static WeakHashMap<Object, EventListenerList> LISTENERS = new WeakHashMap<>();

	public static void addViewChangeListener(Object bean, PropertyViewChangeListener listener) {
		EventListenerList ell = new EventListenerList();
		ell.add(PropertyViewChangeListener.class, listener);
		LISTENERS.put(bean, ell);
	}

	public static void removeViewChangeChangeListener(UpdatableViewBean bean, PropertyViewChangeListener listener) {
		LISTENERS.get(bean).remove(PropertyViewChangeListener.class, listener);
		LISTENERS.remove(bean);
	}

	default void fireUpdateView(UpdatableViewBean bean, String propertyName, boolean reloadValue) {
		FxUtils.runLaterIfNeeded(() -> {
			EventListenerList ell = LISTENERS.get(bean);
			if (ell == null)
				return;
			for (PropertyViewChangeListener listener : ell.getListeners(PropertyViewChangeListener.class))
				listener.viewchanged(propertyName, reloadValue);
		});
	}

	public void updateView();
}

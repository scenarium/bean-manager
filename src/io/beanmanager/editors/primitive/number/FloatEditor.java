/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.primitive.number;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class FloatEditor extends NumberEditor<Float> {
	private float min = getMinTypeValue();
	private float max = getMaxTypeValue();

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new FloatEditor().getCustomEditor(), new FloatEditor(0.00f, 0.01f, ControlType.SPINNER_AND_SLIDER).getCustomEditor()));
	}

	public FloatEditor() {}

	public FloatEditor(float min, float max, ControlType controlType) {
		setMin(min);
		setMax(max);
		setControleType(controlType);
	}

	public FloatEditor(float min, float max, ControlType controlType, float increment, IncrementMode incrementMode) {
		setMin(min); // Ajuster la valeur dans checkbounds
		setMax(max); // Ajuster la valeur dans checkbounds
		setIncrement(increment); // OK rien à faire
		setIncrementMode(incrementMode); // OK rien à faire
		setControleType(controlType); // Refaire l'IHM
	}

	public FloatEditor(Float[] numbers) {
		super();
		setPossibilities(numbers);
	}

	@Override
	protected Float changeValue(Float incrFactor, boolean positive, boolean ctrl, boolean shift) {
		float inc = incrFactor;
		if (shift && ctrl)
			inc /= 100;
		else if (shift)
			inc /= 1000;
		else if (ctrl)
			inc /= 10;
		return getValue() + (positive ? inc : -inc);
	}

	private void checkBounds() {
		// if (max == -Float.MAX_VALUE)
		// max++;
		// if (min == Float.MAX_VALUE)
		// min--;
		// if (max <= min)
		// max = min + 1;
		if (this.max < this.min)
			this.max = this.min;
		updateValueWithBounds();
	}

	@Override
	protected Float defaultValue() {
		return this.min > 0 || this.max < 0 ? this.min : 0;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Float.toString(getValue());
	}

	@Override
	public Float getMax() {
		return this.max;
	}

	@Override
	public Float getMin() {
		return this.min;
	}

	@Override
	public Float getNumberAsGeneric(Number number) {
		return number.floatValue();
	}

	@Override
	protected String getNumberAsText(Float number) {
		return Float.toString(number.floatValue());
	}

	@Override
	public int getPitch() {
		return Float.BYTES;
	}

	@Override
	protected int isInBound(Float val) {
		return val < this.min ? -1 : val > this.max ? 1 : 0;
	}

	@Override
	protected boolean isIntegerNumber() {
		return false;
	}

	@Override
	protected Float parseNumber(String text) {
		return Float.parseFloat(text);
	}

	@Override
	public Float readValue(DataInput raf) throws IOException {
		return raf.readFloat();
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)) ? null : Float.parseFloat(text));
	}

	@Override
	public void setMax(Float max) {
		if (max != null && Double.isInfinite(max))
			throw new IllegalArgumentException("argument must be finite");
		this.max = max == null || Float.isNaN(max) ? Float.MAX_VALUE : max;
		checkBounds();
		resetEditor();
	}

	@Override
	public void setMin(Float min) {
		if (min != null && Double.isInfinite(min))
			throw new IllegalArgumentException("argument must be finite");
		this.min = min == null || Float.isNaN(min) ? -Float.MAX_VALUE : min;
		checkBounds();
		resetEditor();
	}

	@Override
	public void writeValue(DataOutput raf, Float value) throws IOException {
		raf.writeFloat(value);
	}

	@Override
	public Float getMaxTypeValue() {
		return Float.MAX_VALUE;
	}

	@Override
	public Float getMinTypeValue() {
		return -Float.MAX_VALUE;
	}
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.primitive.number;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class LongEditor extends NumberEditor<Long> {
	private long min = getMinTypeValue();
	private long max = getMaxTypeValue();

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new LongEditor().getEditor(), new LongEditor(-100, 1000, ControlType.SPINNER_AND_SLIDER).getEditor()));
	}

	public LongEditor() {}

	public LongEditor(long min, long max, ControlType controlType) {
		setMin(min);
		setMax(max);
		setControleType(controlType);
	}

	public LongEditor(long min, long max, ControlType controlType, long increment, IncrementMode incrementMode) {
		setMin(min);
		setMax(max);
		setIncrement(increment);
		setIncrementMode(incrementMode);
		setControleType(controlType);
	}

	public LongEditor(Long[] numbers) {
		super();
		setPossibilities(numbers);
	}

	@Override
	protected Long changeValue(Long incrFactor, boolean positive, boolean ctrl, boolean shift) {
		long inc = incrFactor;
		if (shift && ctrl)
			inc *= 100;
		else if (shift)
			inc *= 1000;
		else if (ctrl)
			inc *= 10;
		return getValue() + (positive ? inc : -inc);
	}

	private void checkBounds() {
		// if (max == Long.MIN_VALUE)
		// max++;
		// if (min == Long.MAX_VALUE)
		// min--;
		if (this.max < this.min)
			this.max = this.min;
		updateValueWithBounds();
	}

	@Override
	protected Long defaultValue() {
		return this.min > 0 || this.max < 0 ? this.min : 0;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Long.toString(getValue());
	}

	@Override
	public Long getMax() {
		return this.max;
	}

	@Override
	public Long getMin() {
		return this.min;
	}

	@Override
	public Long getNumberAsGeneric(Number number) {
		return number.longValue();
	}

	@Override
	protected String getNumberAsText(Long number) {
		return Long.toString(number);
	}

	@Override
	public int getPitch() {
		return Long.BYTES;
	}

	@Override
	protected int isInBound(Long val) {
		return val < this.min ? -1 : val > this.max ? 1 : 0;
	}

	@Override
	protected boolean isIntegerNumber() {
		return true;
	}

	@Override
	protected Long parseNumber(String text) {
		return Long.parseLong(text);
	}

	@Override
	public Long readValue(DataInput raf) throws IOException {
		return raf.readLong();
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)) ? null : Long.parseLong(text)); // null sinon player timeB pas bon
	}

	@Override
	public void setMax(Long max) {
		this.max = max == null ? Long.MAX_VALUE : max;
		checkBounds();
		resetEditor();
	}

	@Override
	public void setMin(Long min) {
		this.min = min == null ? Long.MIN_VALUE : min;
		checkBounds();
		resetEditor();
	}

	@Override
	public void writeValue(DataOutput raf, Long value) throws IOException {
		raf.writeLong(value);
	}

	@Override
	public Long getMaxTypeValue() {
		return Long.MAX_VALUE;
	}

	@Override
	public Long getMinTypeValue() {
		return Long.MIN_VALUE;
	}
}

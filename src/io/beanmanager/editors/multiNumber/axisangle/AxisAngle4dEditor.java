package io.beanmanager.editors.multiNumber.axisangle;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.AxisAngle4d;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.DoubleEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class AxisAngle4dEditor extends StaticSizeMonoDimensionEditor<AxisAngle4d, Double> {

	public AxisAngle4dEditor() {
		super(4);
	}

	@Override
	protected List<Double> getArrayFromValue(AxisAngle4d value) {
		return List.of(value.x, value.y, value.z, value.angle);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected AxisAngle4d getValueFromArray(List<Double> datas) {
		return new AxisAngle4d(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}

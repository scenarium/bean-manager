package io.beanmanager.editors.multiNumber.axisangle;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.AxisAngle4f;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.FloatEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class AxisAngle4fEditor extends StaticSizeMonoDimensionEditor<AxisAngle4f, Float> {

	public AxisAngle4fEditor() {
		super(4);
	}

	@Override
	protected List<Float> getArrayFromValue(AxisAngle4f value) {
		return List.of(value.x, value.y, value.z, value.angle);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected AxisAngle4f getValueFromArray(List<Float> datas) {
		return new AxisAngle4f(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}

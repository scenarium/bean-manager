/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.multiNumber;

public abstract class StaticSizeBiDimensionEditor<T, U extends Number> extends BiDimensionEditor<T, U> {

	public StaticSizeBiDimensionEditor(int nRow, int nCol) {
		super(nRow, nCol);
	}

	@Override
	protected int[] getDimensions(T object) {
		return new int[] { this.nRow * this.nCol };
	}
}

package io.beanmanager.editors.multiNumber.quaternion;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Quat4f;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.FloatEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class Quat4fEditor extends StaticSizeMonoDimensionEditor<Quat4f, Float> {

	public Quat4fEditor() {
		super(4);
	}

	@Override
	protected List<Float> getArrayFromValue(Quat4f value) {
		return List.of(value.x, value.y, value.z, value.w);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected Quat4f getValueFromArray(List<Float> datas) {
		return new Quat4f(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}

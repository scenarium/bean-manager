package io.beanmanager.editors.multiNumber.quaternion;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Quat4d;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.DoubleEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class Quat4dEditor extends StaticSizeMonoDimensionEditor<Quat4d, Double> {

	public Quat4dEditor() {
		super(4);
	}

	@Override
	protected List<Double> getArrayFromValue(Quat4d value) {
		return List.of(value.x, value.y, value.z, value.w);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected Quat4d getValueFromArray(List<Double> datas) {
		return new Quat4d(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}

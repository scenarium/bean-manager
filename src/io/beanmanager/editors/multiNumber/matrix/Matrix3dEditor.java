/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.multiNumber.matrix;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Matrix3d;

import io.beanmanager.editors.multiNumber.StaticSizeBiDimensionEditor;
import io.beanmanager.editors.primitive.number.DoubleEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class Matrix3dEditor extends StaticSizeBiDimensionEditor<Matrix3d, Double> {
	public Matrix3dEditor() {
		super(3, 3);
	}

	@Override
	protected List<Double> getArrayFromValue(Matrix3d value) {
		return List.of(value.m00, value.m01, value.m02, value.m10, value.m11, value.m12, value.m20, value.m21, value.m22);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected Matrix3d getValueFromArray(List<Double> datas) {
		return new Matrix3d(datas.get(0), datas.get(1), datas.get(2), datas.get(3), datas.get(4), datas.get(5), datas.get(6), datas.get(7), datas.get(8));
	}
}

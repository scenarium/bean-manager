package io.beanmanager.editors.multiNumber.texCoord;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.TexCoord2f;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.FloatEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class TexCoord2fEditor extends StaticSizeMonoDimensionEditor<TexCoord2f, Float> {
	public TexCoord2fEditor() {
		super(2);
	}

	@Override
	protected List<Float> getArrayFromValue(TexCoord2f value) {
		return List.of(value.x, value.y);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected TexCoord2f getValueFromArray(List<Float> datas) {
		return new TexCoord2f(datas.get(0), datas.get(1));
	}
}

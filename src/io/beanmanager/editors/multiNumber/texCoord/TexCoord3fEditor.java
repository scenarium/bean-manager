package io.beanmanager.editors.multiNumber.texCoord;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.TexCoord3f;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.FloatEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class TexCoord3fEditor extends StaticSizeMonoDimensionEditor<TexCoord3f, Float> {
	public TexCoord3fEditor() {
		super(3);
	}

	@Override
	protected List<Float> getArrayFromValue(TexCoord3f value) {
		return List.of(value.x, value.y, value.z);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected TexCoord3f getValueFromArray(List<Float> datas) {
		return new TexCoord3f(datas.get(0), datas.get(1), datas.get(2));
	}
}

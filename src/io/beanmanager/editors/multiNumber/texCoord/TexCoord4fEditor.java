package io.beanmanager.editors.multiNumber.texCoord;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.TexCoord4f;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.FloatEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class TexCoord4fEditor extends StaticSizeMonoDimensionEditor<TexCoord4f, Float> {
	public TexCoord4fEditor() {
		super(4);
	}

	@Override
	protected List<Float> getArrayFromValue(TexCoord4f value) {
		return List.of(value.x, value.y, value.z, value.w);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected TexCoord4f getValueFromArray(List<Float> datas) {
		return new TexCoord4f(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}

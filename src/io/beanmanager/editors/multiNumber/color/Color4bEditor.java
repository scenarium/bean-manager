package io.beanmanager.editors.multiNumber.color;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Color4b;

import io.beanmanager.editors.primitive.number.NumberEditor;
import io.beanmanager.editors.primitive.number.ShortEditor;
import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Color4bEditor extends AbstractColorEditor<Color4b, Short> {

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new Color4bEditor().getCustomEditor()));
	}

	public Color4bEditor() {
		super(4);
	}

	@Override
	protected Color toFxColor(Color4b color) {
		return new Color((color.x & 0xFF) / 255.0, (color.y & 0xFF) / 255.0, (color.z & 0xFF) / 255.0, (color.w & 0xFF) / 255.0);
	}

	@Override
	protected Color4b fromFxColor(Color color) {
		return new Color4b((byte) (color.getRed() * 255.0), (byte) (color.getGreen() * 255.0), (byte) (color.getBlue() * 255.0), (byte) (color.getOpacity() * 255.0));
	}

	@Override
	protected double getScaleFactor() {
		return 255.0;
	}

	@Override
	protected double getEditorPrefWidth() {
		return 30;
	}

	@Override
	protected List<Short> getArrayFromValue(Color4b value) {
		return List.of((short) (value.x & 0xFF), (short) (value.y & 0xFF), (short) (value.z & 0xFF), (short) (value.w & 0xFF));
	}

	@Override
	protected Supplier<NumberEditor<Short>> getEditorConstructor() {
		return ShortEditor::new;
	}

	@Override
	protected Color4b getValueFromArray(List<Short> datas) {
		return new Color4b(datas.get(0).byteValue(), datas.get(1).byteValue(), datas.get(2).byteValue(), datas.get(3).byteValue());
	}

	@Override
	public Color4b readValue(DataInput raf) throws IOException {
		return new Color4b(raf.readByte(), raf.readByte(), raf.readByte(), raf.readByte());
	}

	@Override
	public void writeValue(DataOutput raf, Color4b value) throws IOException {
		raf.writeByte(value.x);
		raf.writeByte(value.y);
		raf.writeByte(value.z);
		raf.writeByte(value.w);
	}
}

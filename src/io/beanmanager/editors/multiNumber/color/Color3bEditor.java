package io.beanmanager.editors.multiNumber.color;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Color3b;

import io.beanmanager.editors.primitive.number.NumberEditor;
import io.beanmanager.editors.primitive.number.ShortEditor;
import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Color3bEditor extends AbstractColorEditor<Color3b, Short> {

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new Color3bEditor().getCustomEditor()));
	}

	public Color3bEditor() {
		super(3);
	}

	@Override
	protected Color toFxColor(Color3b color) {
		return new Color((color.x & 0xFF) / 255.0, (color.y & 0xFF) / 255.0, (color.z & 0xFF) / 255.0, 1);
	}

	@Override
	protected Color3b fromFxColor(Color color) {
		return new Color3b((byte) (color.getRed() * 255.0), (byte) (color.getGreen() * 255.0), (byte) (color.getBlue() * 255.0));
	}

	@Override
	protected double getScaleFactor() {
		return 255.0;
	}

	@Override
	protected double getEditorPrefWidth() {
		return 30;
	}

	@Override
	protected List<Short> getArrayFromValue(Color3b value) {
		return List.of((short) (value.x & 0xFF), (short) (value.y & 0xFF), (short) (value.z & 0xFF));
	}

	@Override
	protected Supplier<NumberEditor<Short>> getEditorConstructor() {
		return ShortEditor::new;
	}

	@Override
	protected Color3b getValueFromArray(List<Short> datas) {
		return new Color3b(datas.get(0).byteValue(), datas.get(1).byteValue(), datas.get(2).byteValue());
	}

	@Override
	public Color3b readValue(DataInput raf) throws IOException {
		return new Color3b(raf.readByte(), raf.readByte(), raf.readByte());
	}

	@Override
	public void writeValue(DataOutput raf, Color3b value) throws IOException {
		raf.writeByte(value.x);
		raf.writeByte(value.y);
		raf.writeByte(value.z);
	}
}

package io.beanmanager.editors.multiNumber.color;

import java.util.List;
import java.util.function.Supplier;

import io.beanmanager.editors.primitive.number.DoubleEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;
import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ColorEditor extends AbstractColorEditor<Color, Double> {

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new ColorEditor().getCustomEditor()));
	}

	public ColorEditor() {
		super(4);
	}

	@Override
	protected Color toFxColor(Color color) {
		return color;
	}

	@Override
	protected Color fromFxColor(Color color) {
		return color;
	}

	@Override
	protected double getScaleFactor() {
		return 1;
	}

	@Override
	protected double getEditorPrefWidth() {
		return -1;
	}

	@Override
	protected List<Double> getArrayFromValue(Color value) {
		return List.of(value.getRed(), value.getGreen(), value.getBlue(), value.getOpacity());
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected Color getValueFromArray(List<Double> datas) {
		return new Color(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}

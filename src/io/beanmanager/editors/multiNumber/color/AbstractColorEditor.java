package io.beanmanager.editors.multiNumber.color;

import java.util.Objects;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.NumberEditor;
import io.beanmanager.tools.FxUtils;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;

public abstract class AbstractColorEditor<T, U extends Number> extends StaticSizeMonoDimensionEditor<T, U> {
	private ColorPicker colorPicker;
	private NumberEditor<?>[] graphicalEditor;

	public AbstractColorEditor(int n) {
		super(n);
	}

	protected abstract Color toFxColor(T color);

	protected abstract T fromFxColor(Color color);

	protected abstract double getScaleFactor();

	protected abstract double getEditorPrefWidth();

	@Override
	protected HBox getCustomEditor() {
		this.graphicalEditor = new NumberEditor[this.getNumberOfValues()];
		HBox hbox = new HBox();
		hbox.setStyle("-fx-background-color: -fx-control-inner-background;" + "-fx-border-width: 1px;" + "-fx-border-color: #969696 #DCDCDC #DCDCDC #969696;");
		hbox.setAlignment(Pos.BOTTOM_CENTER);
		this.colorPicker = new ColorPicker();
		this.colorPicker.setMinWidth(120);
		this.colorPicker.setMinHeight(24);
		this.colorPicker.setOnAction(e -> {
			T newValue = fromFxColor(this.colorPicker.getValue());
			if (!Objects.equals(newValue, getValue()))
				setValue(newValue);
			updateValue();
		});
		this.colorPicker.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				updateValue();
				FxUtils.traverse();
				e.consume();
			}
		});
		hbox.setSpacing(0);
		hbox.getChildren().add(this.colorPicker);
		hbox.setFillHeight(true);
		HBox.setHgrow(this.colorPicker, Priority.ALWAYS);
		double scaleFactor = getScaleFactor();
		for (int i = 0; i < this.graphicalEditor.length; i++) {
			NumberEditor<U> se = getEditorConstructor().get();
			se.setMin(se.getNumberAsGeneric(0));
			se.setMax(se.getNumberAsGeneric(scaleFactor));
			se.setControleType(ControlType.TEXTFIELD);
			se.setStyle("-fx-background-color: transparent;");
			TextField c = (TextField) ((HBox) se.getNoSelectionEditor()).getChildren().get(0);
			double prefWidth = getEditorPrefWidth();
			if (prefWidth > 0)
				c.setPrefWidth(getEditorPrefWidth());
			HBox.setHgrow(c, Priority.ALWAYS);
			c.setPadding(new Insets(0, 1, 0, 1));
			c.setAlignment(Pos.CENTER);
			c.setMinWidth(30);
			hbox.getChildren().add(c);
			if (i != this.graphicalEditor.length - 1) {
				Label l = new Label(",");
				l.setPadding(new Insets(0));
				l.setStyle("-fx-background-color: transparent");
				hbox.getChildren().add(l);
			}
			this.graphicalEditor[i] = se;
		}
		updateCustomEditor();
		for (NumberEditor<?> se : this.graphicalEditor)
			se.addPropertyChangeListener(() -> {
				double[] values = new double[this.graphicalEditor.length];
				for (int i = 0; i < values.length; i++)
					values[i] = ((Number) this.graphicalEditor[i].getValue()).doubleValue();
				T newValue = fromFxColor(new Color(values[0] / scaleFactor, values[1] / scaleFactor, values[2] / scaleFactor, values.length <= 3 ? 1 : values[3] / scaleFactor));
				if (!Objects.equals(newValue, getValue()))
					setValue(newValue);
			});
		addPropertyChangeListener(() -> {
			if (this.colorPicker != null) {
				T value = getValue();
				this.colorPicker.setValue(value == null ? null : toFxColor(value));
			}
		});
		TextField tf = new TextField();
		tf.setStyle("-fx-background-color: transparent;");
		// hbox.setPrefWidth(230);
		return hbox;
	}

	@Override
	public void updateCustomEditor() {
		if (this.colorPicker == null)
			return;
		T value = getValue();
		this.colorPicker.setValue(value == null ? null : toFxColor(value));
		updateValue();
	}

	@Override
	public boolean isFixedControlSized() {
		return false;
	}

	private void updateValue() {
		T value = getValue();
		Color color = value == null ? null : toFxColor(value);
		double scaleFactor = getScaleFactor();
		Number red = this.graphicalEditor[0].getNumberAsGeneric(color.getRed() * scaleFactor);
		if (!this.graphicalEditor[0].getValue().equals(red))
			this.graphicalEditor[0].setValueFromObj(red);
		Number green = this.graphicalEditor[1].getNumberAsGeneric(color.getGreen() * scaleFactor);
		if (!this.graphicalEditor[1].getValue().equals(green))
			this.graphicalEditor[1].setValueFromObj(green);
		Number blue = this.graphicalEditor[2].getNumberAsGeneric(color.getBlue() * scaleFactor);
		if (!this.graphicalEditor[2].getValue().equals(blue))
			this.graphicalEditor[2].setValueFromObj(blue);
		if (this.graphicalEditor.length > 3) {
			Number opacity = this.graphicalEditor[3].getNumberAsGeneric(color.getOpacity() * scaleFactor);
			if (!this.graphicalEditor[3].getValue().equals(opacity))
				this.graphicalEditor[3].setValueFromObj(opacity);
		}
	}
}
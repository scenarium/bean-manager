package io.beanmanager.editors.multiNumber.color;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Color3f;

import io.beanmanager.editors.primitive.number.FloatEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;
import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Color3fEditor extends AbstractColorEditor<Color3f, Float> {

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new Color3fEditor().getCustomEditor()));
	}

	public Color3fEditor() {
		super(3);
	}

	@Override
	protected Color toFxColor(Color3f color) {
		return new Color(color.x, color.y, color.z, 1);
	}

	@Override
	protected Color3f fromFxColor(Color color) {
		return new Color3f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue());
	}

	@Override
	protected double getScaleFactor() {
		return 1;
	}

	@Override
	protected double getEditorPrefWidth() {
		return 83;
	}

	@Override
	protected List<Float> getArrayFromValue(Color3f value) {
		return List.of(value.x, value.y, value.z);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected Color3f getValueFromArray(List<Float> datas) {
		return new Color3f(datas.get(0), datas.get(1), datas.get(2));
	}
}

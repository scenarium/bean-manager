package io.beanmanager.editors.multiNumber.color;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Color4f;

import io.beanmanager.editors.primitive.number.FloatEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;
import io.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Color4fEditor extends AbstractColorEditor<Color4f, Float> {

	public static void main(String[] args) {
		FxTest.launchIHM(args, s -> new VBox(new Color4fEditor().getCustomEditor()));
	}

	public Color4fEditor() {
		super(4);
	}

	@Override
	protected Color toFxColor(Color4f color) {
		return new Color(color.x, color.y, color.z, color.w);
	}

	@Override
	protected Color4f fromFxColor(Color color) {
		return new Color4f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue(), (float) color.getOpacity());
	}

	@Override
	protected double getScaleFactor() {
		return 1;
	}

	@Override
	protected double getEditorPrefWidth() {
		return 83;
	}

	@Override
	protected List<Float> getArrayFromValue(Color4f value) {
		return List.of(value.x, value.y, value.z, value.w);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected Color4f getValueFromArray(List<Float> datas) {
		return new Color4f(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.multiNumber.vector;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Vector2d;

import io.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import io.beanmanager.editors.primitive.number.DoubleEditor;
import io.beanmanager.editors.primitive.number.NumberEditor;

public class Vector2dEditor extends StaticSizeMonoDimensionEditor<Vector2d, Double> {
	public Vector2dEditor() {
		super(2);
	}

	@Override
	protected List<Double> getArrayFromValue(Vector2d value) {
		return List.of(value.x, value.y);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected Vector2d getValueFromArray(List<Double> datas) {
		return new Vector2d(datas.get(0), datas.get(1));
	}
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.editors.multiNumber;

import io.beanmanager.internal.Log;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public abstract class MonoDimensionEditor<T, U extends Number> extends MultiNumberEditor<T, U> {

	protected abstract int getDimension(T object);

	public MonoDimensionEditor(int size) {
		super(size);
	}

	@Override
	protected HBox getCustomEditor() {
		HBox hb = new HBox();
		updateCustomEditor(hb);
		return hb;
	}

	@Override
	public void updateCustomEditor() {
		updateCustomEditor((HBox) customEditor());
	}

	private void updateCustomEditor(HBox hb) {
		if (hb == null)
			return;
		hb.getChildren().clear();
		hb.setAlignment(Pos.CENTER);
		int size = this.editors.size();
		double prefWidth = -2;
		boolean useNames = this.names != null;
		if (useNames && this.names.length != this.editors.size()) {
			Log.error(getClass().getSimpleName() + ": editor sub element names array size (" + this.names.length + ") must be equal to the number of sub elements (" + this.editors.size()
					+ "), so names are ignored");
			useNames = false;
		}
		for (int i = 0; i < size; i++) {
			Region ce = this.editors.get(i).getNoSelectionEditor();
			if (prefWidth == -2)
				prefWidth = ce.getPrefWidth() / 2.0 + Math.max(0, 10 - size) * ce.getPrefWidth() / 20.0;
			ce.setPrefWidth(prefWidth);
			Tooltip.install(ce, new Tooltip(Integer.toString(i)));
			HBox.setHgrow(ce, Priority.ALWAYS);
			if (useNames)
				hb.getChildren().add(new Label(this.names[i] + ":"));
			hb.getChildren().add(ce);
		}
	}

	protected void updateSize(int size) {
		updateNumberOfValue(size);
	}

	@Override
	protected int[] getDimensions() {
		return new int[] { this.editors.size() };
	}

	@Override
	protected void setDimensions(int[] dimensions) {
		updateSize(dimensions[0]);
	}
}

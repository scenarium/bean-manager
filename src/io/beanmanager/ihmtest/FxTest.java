/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.ihmtest;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class FxTest extends Application {

	private static IHMProvider ihmProvider;
	private static EventHandler<WindowEvent> closeEvent;
	private static boolean debugMode;

	public static void launchIHM(String[] args, IHMProvider ihmProvider) {
		FxTest.ihmProvider = ihmProvider;
		launch(args);
	}

	public static void launchIHM(String[] args, IHMProvider ihmProvider, boolean debugMode) {
		FxTest.debugMode = debugMode;
		launchIHM(args, ihmProvider);
	}

	public static void launchIHM(String[] args, IHMProvider ihmProvider, EventHandler<WindowEvent> closeEvent) {
		FxTest.closeEvent = closeEvent;
		launchIHM(args, ihmProvider);
	}

	public static void launchIHM(String[] args, IHMProvider ihmProvider, EventHandler<WindowEvent> closeEvent, boolean debugMode) {
		FxTest.debugMode = debugMode;
		launchIHM(args, ihmProvider);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Tool Test");
		Region region = ihmProvider.getRegion(primaryStage);
		Scene scene = new Scene(region, -1, -1);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		region.prefWidthProperty().addListener(e -> Platform.runLater(() -> primaryStage.sizeToScene()));
		region.prefHeightProperty().addListener(e -> Platform.runLater(() -> primaryStage.sizeToScene()));
		primaryStage.show();
		if (closeEvent != null)
			primaryStage.setOnCloseRequest(closeEvent);
		// if (debugMode)
		// ScenicView.show(scene);
	}
}

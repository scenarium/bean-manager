/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.ihmtest;

import javafx.scene.layout.Region;
import javafx.stage.Stage;

@FunctionalInterface
public interface IHMProvider {
	Region getRegion(Stage stage);
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.rmi.server;

import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.internal.Log;

import javafx.application.Platform;

public class RMIBeanServer implements RMIBeanServerImpl {
	private static final HashMap<String, RemoteBeanHandle<?>> REMOTE_BEANS = new HashMap<>();
	private static boolean verbose = false;
	private static int port = 0;
	private static Registry registery;

	public static void main(String[] args) throws RemoteException {
		launch(args, new RMIBeanServer());
	}

	public static void launch(String[] args, RMIBeanServer server) {
		if (args.length != 3)
			Log.critical(
					"RMIBeanServer must be launch with three arguments:\n-port: the port for the remote bean\n-terminateIfParentTerminate: tell if the RMIBeanServer must terminate if the father process terminate\n-verbose: Explicitly displays in the console all operations performed");
		try {
			port = Integer.parseInt(args[0]);
			if (port < 0 || port > Short.MAX_VALUE * 2 + 1)
				Log.critical("The port property wmust be in the interval [0, " + Short.MAX_VALUE * 2 + 1 + "]");
		} catch (NumberFormatException e) {
			Log.critical("The port property is not formated as an integer");
		}
		try {
			if (Boolean.parseBoolean(args[1]))
				ProcessHandle.current().parent().ifPresent(a -> a.onExit().thenRun(() -> System.exit(1)));
		} catch (Exception e) {
			Log.critical("The port terminateIfParentTerminate is not formated as an boolean");
		}
		verbose = Boolean.parseBoolean(args[2]);
		if (verbose)
			Log.info("Start of RMIBeanServer on port: " + port);
		BeanEditor.needToLoadBrotherBean = false;
		try {
			System.setProperty("java.rmi.server.hostname", InetAddress.getLocalHost().getHostName());
			UnicastRemoteObject.exportObject(server, port);
			registery = LocateRegistry.createRegistry(port);
			registery.bind(Integer.toString(port), server);
			// Naming.rebind("rmi://" + java.net.InetAddress.getLocalHost().getHostName() + ":" + port + "/" + port, server);
			Log.verbose("RMIBeanServer ready on port: " + port);
		} catch (RemoteException | UnknownHostException | IllegalArgumentException | SecurityException | AlreadyBoundException e) {
			Log.error("Server creation failed: " + e);
			// e.printStackTrace();
		}
	}

	protected RMIBeanServer() throws RemoteException {
		super();
	}

	@Override
	public synchronized boolean createRemoteBean(String classDescriptor, String identifier, RMIBeanCallBackImpl callBack, boolean eraseCallBackIfPresent) throws RemoteException {
		try {
			// registery.bind(identifier, obj);
			// String name = "rmi://" + InetAddress.getLocalHost().getHostName() + ":" + port + "/" + port + "_" + identifier;
			RemoteBeanHandle<?> remoteBeanHandle = REMOTE_BEANS.get(identifier);
			if (remoteBeanHandle != null) {
				remoteBeanHandle.cpt++;
				if (eraseCallBackIfPresent)
					remoteBeanHandle.remoteBean.setBeanCallBack(callBack);
			} else {
				Class<?> type = BeanManager.getClassFromDescriptor(classDescriptor);
				RMIBean<?> remoteBean = getRMIObject(type, identifier, callBack);
				REMOTE_BEANS.put(identifier, new RemoteBeanHandle<>(identifier, remoteBean));
				UnicastRemoteObject.exportObject(remoteBean, port);
				registery.bind(identifier, remoteBean);
				// Naming.rebind(name, remoteBean);
				if (verbose)
					Log.info("RemoteBean: " + identifier + " of type: " + type.getCanonicalName() + " created");
			}
			return true;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | ClassNotFoundException
				| AlreadyBoundException e) {
			e.printStackTrace();
			Log.error("RemoteBean creation failed: " + e);
			return false;
		}
	}

	@Override
	public void destroy() throws RemoteException {
		for (RemoteBeanHandle<?> rbh : REMOTE_BEANS.values())
			try {
				registery.unbind(rbh.name);
				UnicastRemoteObject.unexportObject(rbh.remoteBean, true);
				// Naming.unbind(rbh.name);
			} catch (RemoteException | NotBoundException e) {
				Log.error("RemoteBean destruction failed: " + e);
				e.printStackTrace();
			}
		try {
			registery.unbind(Integer.toString(port));
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		UnicastRemoteObject.unexportObject(registery, true);
		Platform.exit();
	}

	@Override
	public synchronized boolean destroyRemoteBean(String identifier) throws RemoteException {
		Log.error("destroyRemoteBean");
		RemoteBeanHandle<?> remoteBeanHandle = REMOTE_BEANS.get(identifier);
		if (remoteBeanHandle != null && --remoteBeanHandle.cpt == 0) {
			var success = new Object() {
				boolean sucess;
			};
			try {
				Naming.unbind(remoteBeanHandle.name);
			} catch (RemoteException | MalformedURLException | NotBoundException e) {
				Log.error("RemoteBean destruction failed: " + e);
				e.printStackTrace();
				success.sucess = false;
			}
			REMOTE_BEANS.remove(identifier);
			if (verbose)
				Log.info("RemoteBean: " + remoteBeanHandle.name.substring(remoteBeanHandle.name.lastIndexOf("_") + 1) + " destructed");
			subBeanGarbageCollector(remoteBeanHandle.remoteBean.bean);
			return success.sucess;
		}
		return true;
	}

	protected <T> RMIBean<T> getRMIObject(Class<T> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		return new RMIBean<>(type, identifier, callBack);
	}

	protected RMIBeanServer getRMIServer() throws RemoteException {
		return new RMIBeanServer();
	}

	private void populateSubBeans(Object fatherBean, HashSet<Object> subBeans) {
		for (Object subBean : BeanManager.getSubBeans(fatherBean, ""))
			if (subBeans.add(subBean))
				populateSubBeans(subBean, subBeans);
	}

	private void subBeanGarbageCollector(Object fatherBean) {
		HashSet<Object> subBeansCandidateToRemote = new HashSet<>();
		subBeansCandidateToRemote.add(fatherBean);
		populateSubBeans(fatherBean, subBeansCandidateToRemote);
		for (RemoteBeanHandle<?> rbh : REMOTE_BEANS.values()) {
			HashSet<Object> otherSubBeans = new HashSet<>();
			otherSubBeans.add(rbh.remoteBean.bean);
			populateSubBeans(rbh.remoteBean.bean, otherSubBeans);
			subBeansCandidateToRemote.removeAll(otherSubBeans);
		}
		subBeansCandidateToRemote.forEach(sbtg -> BeanEditor.unregisterBean(sbtg, false));
	}
}

class RemoteBeanHandle<T> {
	public final String name;
	public final RMIBean<T> remoteBean;
	public int cpt;

	public RemoteBeanHandle(String name, RMIBean<T> remoteBean) {
		this.name = name;
		this.remoteBean = remoteBean;
		this.cpt = 1;
	}
}
/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.rmi.server;

import java.beans.IntrospectionException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.HashMap;

import io.beanmanager.BeanAndSubBeanPropertyChangeListener;
import io.beanmanager.BeanDesc;
import io.beanmanager.BeanManager;
import io.beanmanager.editors.DynamicAnnotationBean;
import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.DynamicVisibleBean;
import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.UpdatableViewBean;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.internal.Log;
import io.beanmanager.rmi.client.RMIBeanAndSubBeanPropertyChangeListenerImpl;
import io.beanmanager.rmi.client.RMIPropertyChangeListenerImpl;

public class RMIBean<T> implements RMIBeanImpl {
	protected RMIBeanCallBackImpl callBack;
	protected T bean;
	private BeanManager bm;
	private HashMap<RMIPropertyChangeListenerImpl, PropertyChangeListener> propertyListenerMap;
	private HashMap<RMIBeanAndSubBeanPropertyChangeListenerImpl, BeanAndSubBeanPropertyChangeListener> beanAndSubBeanListenerMap;
	private HashMap<Class<?>, HashMap<String, PropertyDescriptor>> propertyDescriptorsMap;
	private HashMap<Class<?>, PropertyEditor<?>> propertyEditorsMap;

	public RMIBean(Class<T> beanType, String beanName, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		super();
		this.bean = beanType.getConstructor().newInstance();
		this.callBack = callBack;
		BeanManager.loadBeanMethod = (bean, file) -> {
			try {
				if (file == null)
					Log.error("the file: is null for the bean: " + bean);
				String subBeanName = file.getAbsolutePath();
				subBeanName = subBeanName.substring(subBeanName.lastIndexOf(File.separator) + 1, subBeanName.length() - BeanManager.SERIALIZE_EXT.length());
				subBeanName = subBeanName.substring(subBeanName.indexOf(BeanDesc.SEPARATOR) + 1, subBeanName.length());
				new BeanManager(bean, "").load(new ByteArrayInputStream(callBack.getBean(BeanManager.getDescriptorFromClass(bean.getClass()), subBeanName)), "");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		};
		if (beanName != null) { // Et si il existait déja?
			BeanEditor.registerBean(this.bean, beanName, "");
			new BeanManager(this.bean, "").load();
		}
	}

	@Override
	public void addBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.bm == null)
			this.bm = new BeanManager(this.bean, "");
		if (this.beanAndSubBeanListenerMap == null)
			this.beanAndSubBeanListenerMap = new HashMap<>();
		BeanAndSubBeanPropertyChangeListener localListener = (beanType, beanName, propertyName) -> {
			try {
				listener.rMIBeanAndSubBeanPropertyChanged(beanType, beanName, propertyName);
			} catch (RemoteException ex) {
				ex.printStackTrace();
			}
		};
		this.bm.addBeanAndSubBeanPropertyChangeListener(localListener);
		this.beanAndSubBeanListenerMap.put(listener, localListener);
	}

	@Override
	public void addPropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.bm == null)
			this.bm = new BeanManager(this.bean, "");
		if (this.propertyListenerMap == null)
			this.propertyListenerMap = new HashMap<>();
		PropertyChangeListener localListener = e -> {
			try {
				listener.rmiPropertyChanged(e.getPropertyName());
			} catch (RemoteException ex) {
				ex.printStackTrace();
			}
		};
		this.bm.addPropertyChangeListener(localListener);
		this.propertyListenerMap.put(listener, localListener);
	}

	public RMIBeanCallBackImpl getBeanCallBack() {
		return this.callBack;
	}

	@Override
	public String getBeanClass() throws RemoteException {
		return BeanManager.getDescriptorFromClass(this.bean.getClass());
	}

	private PropertyDescriptor getPropertyDecriptor(Object bean, String propertyName) {
		Class<? extends Object> beanClass = bean.getClass();
		if (this.propertyDescriptorsMap == null)
			this.propertyDescriptorsMap = new HashMap<>();
		HashMap<String, PropertyDescriptor> pds = this.propertyDescriptorsMap.get(beanClass);
		if (pds == null) {
			pds = new HashMap<>();
			this.propertyDescriptorsMap.put(beanClass, pds);
		}
		PropertyDescriptor pd = pds.get(propertyName);
		if (pd != null)
			return pd;
		BeanManager beanManager = new BeanManager(bean, "");
		for (PropertyDescriptor opd : beanManager.getPropertyDescriptors())
			if (opd.getName().equals(propertyName)) {
				pds.put(propertyName, opd);
				return opd;
			}
		return null;
	}

	private PropertyEditor<?> getPropertyEditor(Class<?> beanClass, PropertyDescriptor pd) {
		if (this.propertyEditorsMap == null)
			this.propertyEditorsMap = new HashMap<>();
		PropertyEditor<?> editor = this.propertyEditorsMap.get(beanClass);
		if (editor == null) {
			editor = BeanManager.getPropertyEditor(pd, pd.getPropertyType(), this.bean, "");
			this.propertyEditorsMap.put(beanClass, editor);
		}
		return editor;
	}

	@Override
	public Object getSubBeanValue(Class<?> beanType, String beanName, String propertyName)
			throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object subBean = BeanEditor.getRegisterBean(beanType, beanName).bean;
		return subBean == null ? null : getValue(subBean, propertyName);
	}

	private Object getValue(Object bean, String propertyName) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> beanClass = bean.getClass();
		PropertyDescriptor pd = getPropertyDecriptor(bean, propertyName);
		if (pd == null)
			throw new IllegalArgumentException("No PropertyDescriptor for: " + propertyName + " of class: " + beanClass);
		Object value = pd.getReadMethod().invoke(bean, (Object[]) null);
		Class<?> type = pd.getPropertyType();
		if (!type.isPrimitive() && !Serializable.class.isAssignableFrom(type)) {
			PropertyEditor<?> editor = getPropertyEditor(beanClass, pd);
			editor.setValueFromObj(value);
			value = editor.getAsText();
		}
		return value;
	}

	@Override
	public Object getValue(String propertyName) throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return getValue(this.bean, propertyName);
	}

	@Override
	public Object[] invokeMethod(String methodname, Object... args)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		return (Object[]) this.bean.getClass().getMethod(methodname).invoke(this.bean, args);
	}

	@Override
	public boolean isDynamicAnnotation() throws RemoteException {
		return this.bean instanceof DynamicAnnotationBean;
	}

	@Override
	public boolean isDynamicEnableBean() throws RemoteException {
		return this.bean instanceof DynamicEnableBean;
	}

	@Override
	public boolean isDynamicVisibleBean() throws RemoteException {
		return this.bean instanceof DynamicVisibleBean;
	}

	@Override
	public boolean isUpdatableViewBean() throws RemoteException {
		return this.bean instanceof UpdatableViewBean;
	}

	@Override
	public String remoteToString() throws RemoteException {
		return this.bean.toString();
	}

	@Override
	public void removeBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.bm == null)
			return;
		BeanAndSubBeanPropertyChangeListener localListener = this.beanAndSubBeanListenerMap.get(listener);
		if (localListener != null) {
			this.bm.removeBeanAndSubBeanPropertyChangeListener(localListener);
			if (this.beanAndSubBeanListenerMap.size() == 1) {
				this.beanAndSubBeanListenerMap = null;
				this.bm = null;
			} else
				this.beanAndSubBeanListenerMap.remove(listener);
		}
	}

	@Override
	public void removePropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.bm == null)
			return;
		PropertyChangeListener localListener = this.propertyListenerMap.get(listener);
		if (localListener != null) {
			this.bm.removePropertyChangeListener(localListener);
			if (this.propertyListenerMap.size() == 1) {
				this.propertyListenerMap = null;
				this.bm = null;
			} else
				this.propertyListenerMap.remove(listener);
		}
	}

	public void setBeanCallBack(RMIBeanCallBackImpl callBack) {
		this.callBack = callBack;
	}

	@Override
	public void setEnable() throws RemoteException {
		((DynamicEnableBean) this.bean).setEnable();
	}

	@Override
	public void setSubBeanValue(Class<?> beanType, String beanName, String propertyName, boolean asText, Object value)
			throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object subBean = BeanEditor.getRegisterBean(beanType, beanName).bean;
		if (subBean != null)
			setValue(subBean, propertyName, asText, value);
	}

	private void setValue(Object bean, String propertyName, boolean asText, Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> beanClass = bean.getClass();
		PropertyDescriptor pd = getPropertyDecriptor(bean, propertyName);
		if (pd == null)
			throw new IllegalArgumentException("No PropertyDescriptor for: " + propertyName + " of class: " + beanClass);
		if (asText) {
			PropertyEditor<?> editor = getPropertyEditor(beanClass, pd);
			editor.setAsText((String) value);
			value = editor.getValue();
		}
		pd.getWriteMethod().invoke(bean, value);
	}

	@Override
	public void setValue(String propertyName, boolean asText, Object value)
			throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		setValue(this.bean, propertyName, asText, value);
	}

	@Override
	public void setVisible() throws RemoteException {
		((DynamicVisibleBean) this.bean).setVisible();
	}
}
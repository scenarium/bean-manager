/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.rmi.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIBeanServerImpl extends Remote {
	public boolean createRemoteBean(String classDescriptor, String identifier, RMIBeanCallBackImpl callBack, boolean eraseCallBackIfPresent) throws RemoteException;

	public void destroy() throws RemoteException;

	public boolean destroyRemoteBean(String identifier) throws RemoteException;
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.rmi.server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import io.beanmanager.BeanDesc;
import io.beanmanager.BeanManager;
import io.beanmanager.editors.container.BeanEditor;

public class RMIBeanCallBack extends UnicastRemoteObject implements RMIBeanCallBackImpl {
	private static final long serialVersionUID = 1L;

	public RMIBeanCallBack() throws RemoteException {
		super();
	}

	@Override
	public byte[] getBean(String classDescriptor, String beanName) throws RemoteException {
		Class<?> beanType;
		try {
			beanType = BeanManager.getClassFromDescriptor(classDescriptor);
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
				BeanDesc<?> beanDesc = BeanEditor.getRegisterBean(beanType, beanName);
				if (beanDesc == null)
					throw new IllegalArgumentException("The bean: " + beanName + " does not exit");
				new BeanManager(beanDesc.bean, "").save(baos, "");
				return baos.toByteArray();
			} catch (IOException ex) {
				ex.printStackTrace();
				return null;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.rmi.server;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.Remote;
import java.rmi.RemoteException;

import io.beanmanager.rmi.client.RMIBeanAndSubBeanPropertyChangeListenerImpl;
import io.beanmanager.rmi.client.RMIPropertyChangeListenerImpl;

public interface RMIBeanImpl extends Remote {
	public void addBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException;

	public void addPropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException;

	public String getBeanClass() throws RemoteException;

	public Object getSubBeanValue(Class<?> beanType, String beanName, String propertyName)
			throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;

	public Object getValue(String propertyName) throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;

	public Object[] invokeMethod(String methodname, Object... args)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException;

	public boolean isDynamicAnnotation() throws RemoteException;

	public boolean isDynamicEnableBean() throws RemoteException;

	public boolean isDynamicVisibleBean() throws RemoteException;

	public boolean isUpdatableViewBean() throws RemoteException;

	public String remoteToString() throws RemoteException;

	public void removeBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException;

	public void removePropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException;

	public void setEnable() throws RemoteException;

	public void setSubBeanValue(Class<?> beanType, String beanName, String propertyName, boolean asText, Object value)
			throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;

	public void setValue(String propertyName, boolean asText, Object value) throws RemoteException, IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;

	public void setVisible() throws RemoteException;

	// public void setBeanName(String beanName) throws RemoteException;
}
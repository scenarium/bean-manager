/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.rmi.client;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import io.beanmanager.BeanManager;
import io.beanmanager.rmi.server.RMIBeanCallBack;
import io.beanmanager.rmi.server.RMIBeanImpl;
import io.beanmanager.rmi.server.RMIBeanServer;
import io.beanmanager.rmi.server.RMIBeanServerImpl;
import io.beanmanager.tools.Tuple;

public class RemoteBean implements RemoteBeanImpl {
	private static final ConcurrentHashMap<Integer, RMIBeanServerHandler> REMOTE_SERVER = new ConcurrentHashMap<>();
	private final String identifier;
	private final String hostName;
	private final int port;
	private final Tuple<RMIBeanServerImpl, Registry> rmiBeanServer;
	private final RMIBeanServerHandler processHandler;
	protected RMIBeanImpl remoteBean;
	private boolean hasProcessExit;

	public RemoteBean(Class<?> type, String id, String hostName, int port, boolean createRemoteOperator) throws Exception {
		this(type, id, hostName, port, createRemoteOperator, new RMIBeanCallBack(), false);
	}

	public RemoteBean(Class<?> type, String id, String hostName, int port, boolean createRemoteOperator, int timeout) throws Exception {
		this(type, id, hostName, port, createRemoteOperator, new RMIBeanCallBack(), false, timeout, null);
	}

	public RemoteBean(Class<? extends Object> type, String id, String hostName, int port, boolean createRemoteOperator, RMIBeanCallBack callBack, boolean eraseCallBackIfPresent) throws Exception {
		this(type, id, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, null);
	}

	public RemoteBean(Class<? extends Object> type, String id, String hostName, int port, boolean createRemoteOperator, RMIBeanCallBack callBack, boolean eraseCallBackIfPresent,
			String[] additionnalArguments) throws Exception {
		this(type, id, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, -1, additionnalArguments);
	}

	public RemoteBean(Class<? extends Object> type, String identifier, String hostName, int port, boolean createRemoteOperator, RMIBeanCallBack callBack, boolean eraseCallBackIfPresent, int timeOut,
			String[] additionnalArguments) throws Exception {
		this.identifier = identifier;
		this.hostName = hostName;
		this.port = port;
		if (createRemoteOperator) {
			var processCreationException = new Object() {
				public Exception e = null;
			};
			RMIBeanServerHandler processHandler = REMOTE_SERVER.computeIfAbsent(port, a -> {
				try {
					Class<?> server = getServer();
					String[] processArgs = new String[10 + (additionnalArguments == null ? 0 : additionnalArguments.length)];
					processArgs[0] = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
					processArgs[1] = "-p";
					processArgs[2] = System.getProperty("jdk.module.path");
					processArgs[3] = "-cp";
					processArgs[4] = System.getProperty("java.class.path");
					processArgs[5] = "-m";
					processArgs[6] = server.getModule().getName() + "/" + getServer().getCanonicalName();
					processArgs[7] = Integer.toString(port);
					processArgs[8] = Boolean.toString(true);
					processArgs[9] = Boolean.toString(false);
					if (additionnalArguments != null)
						System.arraycopy(additionnalArguments, 0, processArgs, 10, additionnalArguments.length);
					Process process = new ProcessBuilder().command(processArgs).inheritIO().start();
					return new RMIBeanServerHandler(process, connectToRemoteServer(hostName, port, timeOut));
				} catch (IOException | NotBoundException | InterruptedException e) {
					processCreationException.e = e;
					return null;
				}
			});
			if (processCreationException.e != null)
				throw processCreationException.e;
			processHandler.beanCount.incrementAndGet();
			processHandler.process.onExit().thenApply((p) -> this.hasProcessExit = true);
			this.processHandler = processHandler;
			this.rmiBeanServer = processHandler.rmiBeanServer;
		} else {
			this.processHandler = null;
			this.rmiBeanServer = connectToRemoteServer(hostName, port, timeOut);
		}
		if (!this.rmiBeanServer.getFirst().createRemoteBean(BeanManager.getDescriptorFromClass(type), identifier, callBack, eraseCallBackIfPresent))// , callBack
			throw new Exception("Remote bean creation fail");
		long time = System.currentTimeMillis();
		while (!this.hasProcessExit) {
			try {
				this.remoteBean = (RMIBeanImpl) this.rmiBeanServer.getSecond().lookup(identifier);
				// this.remoteBean = (RMIBeanImpl) Naming.lookup("rmi://" + hostName + ":" + port + "/" + port + "_" + identifier);
				break;
			} catch (NotBoundException | RemoteException e) {
				if (timeOut > 0 && System.currentTimeMillis() - time > timeOut)
					return;
				if (e instanceof InterruptedException)
					break;
			}
			if (Thread.currentThread().isInterrupted())
				throw new InterruptedException();
		}
	}

	@Override
	public void addBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.addBeanAndSubBeanPropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void addPropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.addPropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	public boolean connectionError() {
		return this.hasProcessExit;
	}

	private Tuple<RMIBeanServerImpl, Registry> connectToRemoteServer(String hostName, int port, int timeOut) throws NotBoundException, MalformedURLException, RemoteException, InterruptedException {
		long time = System.currentTimeMillis();
		while (!this.hasProcessExit) {
			try {
				Registry registry = LocateRegistry.getRegistry(port);
				RMIBeanServerImpl val = (RMIBeanServerImpl) registry.lookup(Integer.toString(port));
				return new Tuple<>(val, registry);
				// Log.info("lookup from: " + ProcessHandle.current().pid() + " " + "rmi://" + hostName + ":" + port + "/" + port);
				// return (RMIBeanServerImpl) Naming.lookup("rmi://" + hostName + ":" + port + "/" + port);
			} catch (NotBoundException | RemoteException e) {
				if (e instanceof InterruptedException)
					break;
				if (timeOut > 0 && System.currentTimeMillis() - time > timeOut)
					throw e;
			}
			if (Thread.currentThread().isInterrupted())
				throw new InterruptedException();
			Thread.sleep(100);
		}
		return null;
	}

	public void destroy() {
		if (this.processHandler != null && this.processHandler.beanCount.decrementAndGet() == 0)
			REMOTE_SERVER.compute(this.port, (port, processHandler) -> {

				try {
					processHandler.rmiBeanServer.getFirst().destroy();
				} catch (RemoteException ex) {
					ex.printStackTrace();
				}

				Process process = processHandler.process;
				process.destroy();
				try {
					process.onExit().get();
				} catch (InterruptedException | ExecutionException e) {
					process.destroyForcibly();
					try {
						process.onExit().get();
					} catch (InterruptedException | ExecutionException ex) {
						ex.printStackTrace();
					}
					e.printStackTrace();
				}
				process = null;
				return null;
			});
		else
			try {
				this.rmiBeanServer.getFirst().destroyRemoteBean(this.identifier);
				// TODO
				// java.rmi.UnmarshalException: Error unmarshaling return header; nested exception is:
				// java.io.EOFException
				// at java.rmi/sun.rmi.transport.StreamRemoteCall.executeCall(StreamRemoteCall.java:236)
				// at java.rmi/sun.rmi.server.UnicastRef.invoke(UnicastRef.java:161)
				// at java.rmi/java.rmi.server.RemoteObjectInvocationHandler.invokeRemoteMethod(RemoteObjectInvocationHandler.java:209)
				// at java.rmi/java.rmi.server.RemoteObjectInvocationHandler.invoke(RemoteObjectInvocationHandler.java:161)
				// at com.sun.proxy.$Proxy17.destroyRemoteBean(Unknown Source)
				// at BeanManager/rmi.client.RemoteBean.destroy(RemoteBean.java:212)
				// at ScenariumFx/fileManager.scenario.dataFlowDiagram.DiagramScheduler.destroyOperator(DiagramScheduler.java:700)
				// at ScenariumFx/fileManager.scenario.dataFlowDiagram.MultiCoreDiagramScheduler$ThreadedBlock.run(MultiCoreDiagramScheduler.java:461)
				// Caused by: java.io.EOFException
				// at java.base/java.io.DataInputStream.readByte(DataInputStream.java:272)
				// at java.rmi/sun.rmi.transport.StreamRemoteCall.executeCall(StreamRemoteCall.java:222)
				// ... 7 more

			} catch (RemoteException e) {
				e.printStackTrace();
			}
		// Log.error("end of destruction");
		this.remoteBean = null;
	}

	public InetSocketAddress getAddress() {
		return new InetSocketAddress(this.hostName, this.port);
	}

	public String getBeanClass() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.getBeanClass();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected Class<?> getServer() {
		return RMIBeanServer.class;
	}

	public Object getSubBeanValue(Class<?> beanType, String beanName, String propertyName) {
		try {
			return this.remoteBean == null ? null : this.remoteBean.getSubBeanValue(beanType, beanName, propertyName);
		} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object getValue(String propertyName) {
		try {
			return this.remoteBean == null ? null : this.remoteBean.getValue(propertyName);
		} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object[] invokeMethod(String methodname, Object... args) {
		try {
			return this.remoteBean == null ? null : this.remoteBean.invokeMethod(methodname, args);
		} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean isConnected() {
		return this.remoteBean != null;
	}

	public boolean isDynamicAnnotation() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isDynamicAnnotation();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isDynamicEnableBean() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isDynamicEnableBean();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isDynamicVisibleBean() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isDynamicVisibleBean();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isUpdatableViewBean() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isUpdatableViewBean();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void removeBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.removeBeanAndSubBeanPropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removePropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.removePropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	public void setEnable() {
		if (this.remoteBean != null)
			try {
				this.remoteBean.setEnable();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	public void setSubBeanValue(Class<?> beanType, String beanName, String propertyName, boolean asText, Object value) {
		if (this.remoteBean != null)
			try {
				this.remoteBean.setSubBeanValue(beanType, beanName, propertyName, asText, value);
			} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
				e.printStackTrace();
			}
	}

	public void setValue(String propertyName, boolean asText, Object value) {
		if (this.remoteBean != null)
			try {
				this.remoteBean.setValue(propertyName, asText, value);
			} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
				e.printStackTrace();
			}
	}

	@Override
	public String toString() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.remoteToString();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}
}

class RMIBeanServerHandler {
	public final Process process;
	public final Tuple<RMIBeanServerImpl, Registry> rmiBeanServer;
	public AtomicInteger beanCount;

	public RMIBeanServerHandler(Process process, Tuple<RMIBeanServerImpl, Registry> tuple) {
		this.process = process;
		this.rmiBeanServer = tuple;
		this.beanCount = new AtomicInteger(0);
	}
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.rmi;

import java.net.InetAddress;
import java.net.UnknownHostException;

import io.beanmanager.internal.Log;
import io.beanmanager.rmi.client.RemoteBean;

public class BeanTest {
	public static void main(String[] args) {
		try {
			testRemoteBean();
			for (int i = 0; i < 1000; i++) {
				Log.info("gc " + i);
				System.gc();
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void testRemoteBean() throws UnknownHostException, Exception {
		RemoteBean remoteBean = new RemoteBean(BeanTest.class, "Bean", InetAddress.getLocalHost().getHostName(), 1100, true);
		remoteBean.setValue("patate", false, 5);
		Log.info(remoteBean.getClass() + " " + remoteBean.toString() + remoteBean.getValue("patate"));
		remoteBean.destroy();
	}

	int patate = 5;

	public int getPatate() {
		return this.patate;
	}

	public void setPatate(int patate) {
		this.patate = patate + 2;
	}

	@Override
	public String toString() {
		return "Je suis beanTest";
	}
}

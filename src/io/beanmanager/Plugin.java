package io.beanmanager;

import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.beanmanager.consumer.EditorConsumer;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.internal.Log;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier {
	@Override
	public void birth() {
		// We must do this kind of things to be sure that no external dynamic module have create the class
		// in his path, and the instance in well managed by the plug-in.
		try {
			Class.forName(PropertyEditorManager.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void loadPlugin(Object pluginInterface) {
		Log.print("Load the bean plugin on " + pluginInterface.getClass().getCanonicalName());
		if (pluginInterface instanceof PluginsBeanSupplier) {
			PluginsBeanSupplier plugins = (PluginsBeanSupplier) pluginInterface;
			// Load generic editors
			plugins.populateEditors(new EditorConsumer());
			// Load name redirection for generic class
			plugins.populateClassNameRedirection(new ClassNameRedirectionConsumer());
		}
	}

	@Override
	public void registerModule(Module module) {
		BeanManager.registerModule(module);
	}

	@Override
	public void unregisterModule(Module module) {
		// I) Operators
		BeanManager.unregisterModule(module);
		PropertyEditorManager.purgeEditors(module);
	}

	// TODO REFACTO This is really interesting need to set it back again ...

	// private static void safelyReloadModules(List<SimpleModuleDescriptor> modules, Runnable unregisterTask) {
	// Log.info("Reload modules: " + modules.stream().map(m -> m.descriptor.toNameAndVersion()).collect(Collectors.joining(",")));
	// Log.todo(" not implemented");
	// List<Runnable> tasks = modules.stream().flatMap(smd -> ModuleManager.fireModuleModified(LOADED_MODULES.get(smd.descriptor.name()).getFirst())).collect(Collectors.toList());
	// // Records all beans that's going to be removed
	// class BeanInfo {
	// String className;
	// String name;
	// String local;
	// byte[] data;
	//
	// public BeanInfo(String className, String name, String local, byte[] data) {
	// this.className = className;
	// this.name = name;
	// this.local = local;
	// this.data = data;
	// }
	// }
	// HashMap<String, List<BeanInfo>> beansToRemoveInfo = new HashMap<>();
	// for (SimpleModuleDescriptor smd : modules) {
	// Module module = LOADED_MODULES.get(smd.descriptor.name()).getFirst();
	// for (BeanDesc<?> beanDesc : BeanEditor.getAllBeans())
	// if (beanDesc.bean.getClass().getModule().equals(module)) {
	// ByteArrayOutputStream baos = new ByteArrayOutputStream();
	// try {
	// new BeanManager(beanDesc.bean, BeanManager.defaultDir).save(baos, "");
	// String beanClassName = BeanManager.getDescriptorFromClass(beanDesc.bean.getClass());
	// List<BeanInfo> sameTypeBeansToRemove = beansToRemoveInfo.get(beanClassName);
	// if (sameTypeBeansToRemove == null) {
	// sameTypeBeansToRemove = new ArrayList<>();
	// beansToRemoveInfo.put(beanClassName, sameTypeBeansToRemove);
	// }
	// sameTypeBeansToRemove.add(new BeanInfo(beanClassName, beanDesc.name, beanDesc.local, baos.toByteArray()));
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }
	// }
	// modules.forEach(smd -> unregisterModule(LOADED_MODULES.get(smd.descriptor.name()).getFirst()));
	// if (unregisterTask != null)
	// unregisterTask.run();
	// // Reload and recreate all removed beans
	// BiFunction<Object, File, Boolean> oldLoadBeanMethod = BeanManager.loadBeanMethod;
	// BeanManager.loadBeanMethod = (bean, file) -> {
	// String subBeanName = file.getAbsolutePath();
	// subBeanName = subBeanName.substring(subBeanName.lastIndexOf(File.separator) + 1, subBeanName.length() - BeanManager.SERIALIZE_EXT.length());
	// subBeanName = subBeanName.substring(subBeanName.indexOf(BeanDesc.SEPARATOR) + 1, subBeanName.length());
	// List<BeanInfo> sameTypeBeansRemoved = beansToRemoveInfo.get(BeanManager.getDescriptorFromClass(bean.getClass()));
	// if (sameTypeBeansRemoved != null)
	// for (Iterator<BeanInfo> iterator = sameTypeBeansRemoved.iterator(); iterator.hasNext();) {
	// BeanInfo beanRemovedInfo = iterator.next();
	// if (beanRemovedInfo.name.equals(subBeanName)) {
	// try {
	// new BeanManager(bean, beanRemovedInfo.local).load(new ByteArrayInputStream(beanRemovedInfo.data), beanRemovedInfo.local);
	// iterator.remove();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return true;
	// }
	// }
	// try {
	// return oldLoadBeanMethod != null ? oldLoadBeanMethod.apply(bean, file) : new BeanManager(bean, "").load(new FileInputStream(file), file.getParent() + File.separator);
	// } catch (IOException e) {
	// e.printStackTrace();
	// return false;
	// }
	// };
	// modules.forEach(smd -> registerModule(smd).isPresent());
	// for (List<BeanInfo> beanRemovedInfos : beansToRemoveInfo.values())
	// for (Iterator<BeanInfo> iterator = beanRemovedInfos.iterator(); iterator.hasNext();) {
	// BeanInfo beanRemovedInfo = iterator.next();
	// try {
	// Class<?> c = BeanManager.getClassFromDescriptor(beanRemovedInfo.className);
	// BeanDesc<?> beanDesc = BeanEditor.getRegisterBean(c, beanRemovedInfo.name);
	// if (beanDesc != null)
	// new BeanManager(beanDesc.bean, beanDesc.local).load(new ByteArrayInputStream(beanRemovedInfo.data), beanDesc.local);
	// else {
	// Object newBean = c.getConstructor().newInstance();
	// new BeanManager(newBean, beanRemovedInfo.local).load(new ByteArrayInputStream(beanRemovedInfo.data), beanRemovedInfo.local);
	// BeanEditor.registerBean(newBean, beanRemovedInfo.name, beanRemovedInfo.local);
	// }
	// iterator.remove();
	// } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
	// | SecurityException e) {
	// Log.error("Cannot reload bean: " + beanRemovedInfo + " due to: " + e.getClass().getSimpleName() + ": " + e.getMessage());
	// }
	// }
	// BeanManager.loadBeanMethod = oldLoadBeanMethod;
	// tasks.forEach(Runnable::run);
	// }
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.struct;

import java.util.Objects;

public class BooleanProperty {
	public final String name;
	public boolean value;

	public BooleanProperty(String name, boolean value) {
		this.name = name;
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof BooleanProperty ? ((BooleanProperty) obj).name.equals(this.name) && this.value == ((BooleanProperty) obj).value : false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.name, this.value);
	}

	@Override
	public String toString() {
		return this.name + ": " + this.value;
	}
}

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.struct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class TreeNode<T> {
	protected T value;
	protected ArrayList<TreeNode<T>> children;

	public TreeNode() {}

	public TreeNode(T value) {
		this.value = value;
	}

	public TreeNode(T value, ArrayList<TreeNode<T>> children) {
		this.value = value;
		this.children = children;
	}

	public void addChild(TreeNode<T> child) {
		if (this.children == null)
			this.children = new ArrayList<>();
		this.children.add(child);
	}

	public void addChildren(Collection<TreeNode<T>> children) {
		if (this.children == null)
			this.children = new ArrayList<>(children);
		else
			this.children.addAll(children);
	}

	public T getValue() {
		return this.value;
	}

	public TreeNode<T> getChild(T child) {
		for (TreeNode<T> treeNode : this.children)
			if (child == treeNode.getValue())
				return treeNode;
		return null;
	}

	public ArrayList<TreeNode<T>> getChildren() {
		return this.children;
	}

	public void removeChild(TreeNode<T> child) {
		this.children.remove(child);
	}

	public void setValue(T value) {
		this.value = value;
	}

	public void setChildren(ArrayList<TreeNode<T>> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return (this.value == null ? "" : this.value) + (this.children == null ? "" : " {" + Arrays.deepToString(this.children.toArray()) + "}");
	}
}
package io.beanmanager;

import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.beanmanager.consumer.EditorConsumer;

public interface PluginsBeanSupplier {

	default void populateEditors(EditorConsumer editorConsumer) {}

	default void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {}
}

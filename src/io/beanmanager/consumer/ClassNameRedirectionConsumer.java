package io.beanmanager.consumer;

import io.beanmanager.BeanManager;

public class ClassNameRedirectionConsumer {
	public boolean accept(String oldClassName, String newClassName) {
		return BeanManager.registerClassNameRedirection(oldClassName, newClassName);
	}
}

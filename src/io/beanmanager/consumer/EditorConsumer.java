/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.beanmanager.consumer;

import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.PropertyEditorManager;

public class EditorConsumer {
	public <T, U extends PropertyEditor<T>> void accept(Class<T> typeClass, Class<U> editorClass) {
		PropertyEditorManager.registerEditor(typeClass, editorClass);
	}
}

import io.beanmanager.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

/******************************************************************************* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors: Revilloud Marc - initial API and implementation ******************************************************************************/
open module io.beanmanager {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	exports io.beanmanager;
	exports io.beanmanager.consumer;
	exports io.beanmanager.editors;
	exports io.beanmanager.editors.basic;
	exports io.beanmanager.editors.container;
	exports io.beanmanager.editors.multiNumber;
	exports io.beanmanager.editors.multiNumber.matrix;
	exports io.beanmanager.editors.multiNumber.point;
	exports io.beanmanager.editors.multiNumber.vector;
	exports io.beanmanager.editors.primitive;
	exports io.beanmanager.editors.primitive.number;
	exports io.beanmanager.editors.time;
	exports io.beanmanager.ihmtest;
	exports io.beanmanager.rmi;
	exports io.beanmanager.rmi.client;
	exports io.beanmanager.rmi.server;
	exports io.beanmanager.struct;
	exports io.beanmanager.testbean;
	exports io.beanmanager.tools;

	requires transitive io.scenarium.pluginmanager;
	requires transitive vecmath;
	requires javafx.graphics;
	requires transitive java.desktop;
	// requires org.scenicview.scenicview;
	requires java.rmi;
	requires transitive javafx.controls;
	requires java.base;
}